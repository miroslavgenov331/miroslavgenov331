# for i in range(1,4):
#     print(i)


def goldbach(my_number):

    primes_list = []
    number_range_list = []

    for i in range(1, my_number+1):
        number_range_list.append(i)

    # print(number_range_list)

    for i in number_range_list:
        if is_prime(i):
            primes_list.append(i)

    if primes_list:
        if primes_list[-1] == my_number:
            primes_list.remove(my_number)
        if not primes_list:
            return "Not enough primes"
    else:
        return "Not enough primes"

   
    if len(primes_list) == 1:
        return "Not enough primes"

    else:

        equals_to_zero = []

        for i in primes_list:
            for j in primes_list:
                if i+j - my_number == 0:
                    equals_to_zero.append(i)
                    equals_to_zero.append(j)
                    # print(i,"  ",j)

        # print()
        # print(equals_to_zero)

        number_etz_size = len(equals_to_zero)

        my_number_list_with_tuple = []

        if number_etz_size > 2:
            # print("yes")
            # print((number_etz_size))
            # print(4//2 % 2)
            if not number_etz_size//2 % 2:
                number_etz_size = number_etz_size//2
                # print(number_etz_size)
                # print(equals_to_zero)
                
                for i in range(1, number_etz_size, 2):
                    if equals_to_zero[i-1] > equals_to_zero[i]:
                        # print(equals_to_zero[i-1],">",equals_to_zero[i])
                        number_for_tuple = (
                            equals_to_zero[i], equals_to_zero[i-1])
                        my_number_list_with_tuple.append(number_for_tuple)
                    elif equals_to_zero[i-1] == equals_to_zero[i]:
                        # print(equals_to_zero[i-1],"==",equals_to_zero[i])
                        number_for_tuple = (
                            equals_to_zero[i-1], equals_to_zero[i])
                        my_number_list_with_tuple.append(number_for_tuple)
                    else:
                        # print(equals_to_zero[i-1],"<",equals_to_zero[i])
                        number_for_tuple = (
                            equals_to_zero[i-1], equals_to_zero[i])
                        my_number_list_with_tuple.append(number_for_tuple)

            else:
                number_etz_size = number_etz_size//2+1
                for i in range(1, number_etz_size, 2):
                    if equals_to_zero[i-1] > equals_to_zero[i]:
                        # print(equals_to_zero[i-1],">",equals_to_zero[i])
                        number_for_tuple = (
                            equals_to_zero[i], equals_to_zero[i-1])
                        my_number_list_with_tuple.append(number_for_tuple)
                    elif equals_to_zero[i-1] == equals_to_zero[i]:
                        # print(equals_to_zero[i-1],"==",equals_to_zero[i])
                        number_for_tuple = (
                            equals_to_zero[i-1], equals_to_zero[i])
                        my_number_list_with_tuple.append(number_for_tuple)
                    else:
                        # print(equals_to_zero[i-1],"<",equals_to_zero[i])
                        number_for_tuple = (
                            equals_to_zero[i-1], equals_to_zero[i])
                        my_number_list_with_tuple.append(number_for_tuple)
                # print("need to add one")

        else:
            for i in range(1, len(equals_to_zero)):
                if equals_to_zero[i-1] > equals_to_zero[i]:
                    # print(equals_to_zero[i-1],">",equals_to_zero[i])
                    number_for_tuple = (equals_to_zero[i], equals_to_zero[i-1])
                    my_number_list_with_tuple.append(number_for_tuple)
                elif equals_to_zero[i-1] == equals_to_zero[i]:
                    # print(equals_to_zero[i-1],"==",equals_to_zero[i])
                    number_for_tuple = (equals_to_zero[i-1], equals_to_zero[i])
                    my_number_list_with_tuple.append(number_for_tuple)
                else:
                    # print(equals_to_zero[i-1],"<",equals_to_zero[i])
                    number_for_tuple = (equals_to_zero[i-1], equals_to_zero[i])
                    my_number_list_with_tuple.append(number_for_tuple)
            # print(equals_to_zero)

        return my_number_list_with_tuple


def is_prime(number):
    if number == 1:
        return False

    for i in range(2, number):
        if number % i == 0:
            return False
    return True

    # print("i= ",i," ,3 i",3%i)

# print(is_prime(1)) # return false
# print(is_prime(2)) # return true


print(goldbach(4))
# [(2,2)]
print(goldbach(6))
# [(3,3)]
print(goldbach(8))
# [(3,5)]
print(goldbach(10))
# [(3,7), (5,5)]
print(goldbach(100))
# [(3, 97), (11, 89), (17, 83), (29, 71), (41, 59), (47, 53)]
