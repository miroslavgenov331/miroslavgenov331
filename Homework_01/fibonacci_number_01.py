# 4.Fibonacci number


def to_number(digits):
    num_fib = 0

    digit_list = []
    count = 0
    reverse_list = []

    for i in range(len(digits)):

        while digits[i]:
            count += 1
            digit_list.append(digits[i] % 10)
            digits[i] = digits[i]//10
        # while

        if count > 1:
            for i in range(len(digit_list), len(digit_list)-count, -1):
                reverse_list.append(digit_list[i-1])
            # for i

            for i in range(len(reverse_list)):
                digit_list[len(digit_list)-len(reverse_list) +
                           i] = reverse_list[i]
            reverse_list = []
            # for i

        # if

        count = 0
    # for i

    # pravi digits_list v int
    for i in range(len(digit_list)):
        num_fib = num_fib*10
        num_fib = num_fib+digit_list[i]
    # for

    return num_fib
# def to_number(digits)


def fib_number(number):
    number_prev = 0
    number_current = 1
    number_nex = 0
    fib_list = []

    # tyk se izvurshva algorituma na fibonacci
    for _ in range(number):
        fib_list.append(number_current)
        number_nex = number_prev+number_current
        number_prev = number_current
        number_current = number_nex
    # for

    # return (fib_list)

    return to_number(fib_list)
# def fib_number(num)


print(fib_number(3))
print(fib_number(10))
