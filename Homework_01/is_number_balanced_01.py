# 5. Is Number Balanceed

def to_digits(number):
    digit_list = []
    digit_list_revers = []

    while number:
        digit_list.append(number % 10)
        number = number//10

    # obrushta masiva
    for i in range(len(digit_list), 0, -1):
        digit_list_revers.append(digit_list[i-1])
    # for

    return digit_list_revers
# to_digits(num)


def is_number_balanced(number):

    arr_len = 0
    digits = []
    digits = to_digits(number)
    arr_len = len(digits)

    # ako duljinata e po golqma to ima poveche ot 1 chislo
    if len(digits) > 1:
        sum_left = 0
        sum_right = 0

        # ako e 0 oznachava che ima cheten broi cifri
        if len(digits) % 2 == 0:

            # razdelq se prisvoenata duljina na 2
            arr_len = arr_len//2
            for i in range(arr_len):
                sum_left = sum_left+digits[i]
            # for

            for i in range(arr_len, len(digits), 1):
                sum_right = sum_right+digits[i]
            # for

            # proverqvame sumata dali e ravna
            if sum_left == sum_right:
                return True
            else:
                return False
        # if

        # ako ne e 0
        else:
            arr_len = arr_len//2

            for i in range(arr_len):
                sum_left = sum_left+digits[i]
            # for

            # arr_index+1 za da se preskochi s 1 index ot sredata
            for i in range(arr_len+1, len(digits), 1):
                sum_right = sum_right+digits[i]
            # for

            # proverqvame sumata dali e ravna
            if sum_left == sum_right:
                return True
            else:
                return False
        # else
    # if

    # ako chisloto e edinichno
    else:
        return True
# is_number_balanced(num)


# test
print(is_number_balanced(9))
print(is_number_balanced(4581))
print(is_number_balanced(28471))
print(is_number_balanced(1238033))
