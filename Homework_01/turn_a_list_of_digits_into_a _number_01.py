# 3.Turn a list of digits into a number

def to_digits(number):

    # count shte e kato broqch kojto shte se izpolzva za diapazon
    # kogato se obrushtat chislata koito sa po dvoiki
    count = 0

    array = []

    arr_reverse = []

   # za da move da razdrobim chislata koit ne sa edinichni kato na primer 21,2,33
    for i in range(len(number)):

        # ako chisloto e 0 shte go dobavi vi v masiva
        if number[i] == 0:
            count += 1
            array.append(number[i])

        # dokato ostatuka ne e 0
        while number[i] :
            count += 1
            array.append(number[i] % 10)
            number[i] = number[i]//10
        # while

        # ako count>1 tova oznachava che v while num  e bilo s 2 civri (12)
        # ,a ne samos edna cifra naprimer samo (3)
        if count > 1:
            # tyk se polzva veche count i za granica se vzima dujinata na arr_reverse
            # v nachaloto shte e 0 no posle shte se uvelichi kakto shte se yvelichi i count

            for i in range(len(array), len(arr_reverse), -1):
                arr_reverse.append(array[i-1])
            # for

        # ako count e <1 tova oznachava che ima samo edinichna cifra naprimer (1) a ne (12)
        # i shte restartirame count da broi ot 0
        else:
            count = 0
        # if else
    # for

    if count == 0:
        arr_reverse = array
    # if

    return arr_reverse
# to_digits(num)


def to_number(digits):
    number = 0
    new_digits = to_digits(digits)

    for i in range(len(new_digits)):
        number = number*10
        number = number+new_digits[i]
    # for

    return number
# to_number(digits)


print(to_number([1,2,3]))
print(to_number([9,9,9,9,9]))
print(to_number([1,2,3,0,2,3]))
print(to_number([21, 2, 33]))
