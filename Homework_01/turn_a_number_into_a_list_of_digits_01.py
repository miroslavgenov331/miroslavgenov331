# 2.Turn a number in a list of digits

def to_digits(number):

    # list
    my_list = []
    my_list_reverse = []

    # while num is not 0
    while number:
        # whill add the remainder in my_list
        my_list.append(number % 10)
        number = number//10
    # while

    # revers the list
    for i in range(len(my_list), 0, -1):
        my_list_reverse.append(my_list[i-1])
    # for

    return my_list_reverse
# to_digits(num)


print(to_digits(123))
print(to_digits(99999))
print(to_digits(123023))
