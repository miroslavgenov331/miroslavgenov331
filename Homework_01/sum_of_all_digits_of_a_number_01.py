# 1. Sum of all digits of a number.py

def sum_of_digits(number):

    # saving the sum
    sum_result = 0

    # if the number is negative it will change it to positive
    if number < 0:
        number = -number
    # if

    # while the number is not 0 do ...
    while number:
        # will save the remainder in sum_result
        sum_result = sum_result+number % 10
        # will divide the number to get the next digit
        number = number//10
    # while

    # return sum_result
    return sum_result
# def sum_of_digits(num)


# test
print(sum_of_digits(1325132435356))
print(sum_of_digits(123))
print(sum_of_digits(6))
print(sum_of_digits(-10))
