# 6.Decreasing sequnce

def is_decreasing(sequence):
    # will save bools to check if the arr is monotonously decreasing
    list_check = []

    # for loop
    for i in range(1, len(sequence), 1):
        # arr[i-1] is the previous variable
        # arr[i] is the next variable
        # and here will compare if a>b
        if sequence[i-1] > sequence[i]:
            list_check.append(True)
        else:
            list_check.append(False)
    # end for

    for i in range(len(list_check)):
        if not list_check[i] == True:
            # if is not True will return false
            return False
        # if
    # end for

    # will return True if monotonously decreasing
    return True
# def is_decreasing(arr)


print(is_decreasing([5, 4, 3, 2, 1]))
print(is_decreasing([1, 2, 3]))
print(is_decreasing([100, 50, 20]))
print(is_decreasing([1, 1, 1, 1]))
