from error_class import EmptyPetList


class Entity:
    """This is the entity class parent of Player , enemy , pet

    Atributes:

        __name(str): name of player , pet , enemy 

        __health_points(int):  health point

        __damage_points(int):  damage point

        __health_points_range(int): health reange need to be equivalent to hp

        __INCREASE(int): this will increase the hp,dmg  default(1.5)

        __level(int): level of pet,player default(0)

        __pet_list(list):  list of with pet or enemy pet default([])


    Methods:

        __init__(name, health_points, damage_points):
            Constructor

        show_name_hp_dmg():
            Show the damage , health and name

        show_pets_for_fight():
            Show the each pet in the list inversed for the fight

        show_pets():
            Show the each pet in the list

        is_alive():
            Show if entity is alive

        take_dmg(dmg):
            This function will decrease health points

        attack(object, dmg):
            This function will attack the object

        get_pet_list():
            return pet list

        get_increase():
            return the increase value 

        get_level():
            return the level

        get_hp_range():
            return hp range

        get_hp():
            return health point

        get_dmg():
            return damage point

        get_name():
            return name

        remove_pet_from_pet_list(curent_pet):
            remove the curent pet from pet_list

        add_to_pet_list(pet):
            add pet to the pet_list

        set_pet_list(this_list):
            set the pet list

        set_name(name):
            set the name

        set_level(value):
            set the level

        set_increase(value):
            set the increse

        set_hp_range(value):
            set  health point range

        set_hp(hp):
            set heal ponts

        set_dmg(dmg):
            set dmg

        __str__():
            return stats win name hp dmg

        restore_player_and_pets_hp(self):
            This function will restor the health and of player and pets if have
    """

    __name = str()
    __health_points = int()
    __damage_points = int()

    __INCREASE = 1.5
    __level = 0
    __pet_list = []

    def __init__(self, name, health_points, damage_points):
        """Constructor

        This constructor have default value 

        self.__INCREASE = 1.5
        self.__level = 0
        self.__pet_list = []

        Args:
            name (str): name
            health_points (int): health point
            damage_points (int): damage point
        """

        self.set_name(name)
        self.set_hp(health_points)
        self.set_dmg(damage_points)
        self.__health_points_range = int(self.get_hp())
        self.__INCREASE = float(1.5)
        self.__pet_list = []

    def show_name_hp_dmg(self):
        """Show the damage , health and name
        """
        print("name: {},hp: {},dmg: {}".format(
            self.get_name(), self.get_hp(), self.get_dmg()))

    def show_pets_for_fight(self):
        """Show the each pet in the list inversed for the fight

        """
        pet_index = len(self.get_pet_list())
        for i in self.get_pet_list()[::-1]:
            print(pet_index, i)
            pet_index -= 1

    def show_pets(self):
        """Show the each pet in the list
        """
        for i, v in enumerate(self.get_pet_list()):  # [::-1]):
            print(i+1, v)

    def is_alive(self):
        """Show if entity is alive

        Returns:
            bool: if alive return True else False
        """
        return not self.get_hp() == 0

    def take_dmg(self, dmg):
        """This function will decrease health points

        Args:
            dmg (int): this is demage point wich will decrease the health
        """

        if dmg >= self.get_hp():
            self.set_hp(0)
        else:
            self.set_hp(self.get_hp()-dmg)

    def attack(self, object, dmg):
        """This function will attack the object

        Args:
            object (Enemy,EnemyPet): this is the target ot attackge by player
            dmg (int): this is damage point this will be given in take_dmg
        """
        # player inherit this attack and will receive expr if object is dead
        if object.is_alive():
            object.take_dmg(dmg, self)
            # print("object is alive")
        # else:
        #     print("object is dead")

    def get_pet_list(self):
        """return pet list

        Returns:
            list: list with pet
        """
        return self.__pet_list

    def get_increase(self):
        """return the increase value 

        Returns:
            int: this is the value to increase the level , hp , exp , gold , ranges
        """
        return self.__INCREASE

    def get_level(self):
        """return the level

        Returns:
            int: this is the level of player
        """
        return self.__level

    def get_hp_range(self):
        """return hp range

        Health point range will be used to heal when player is in castle
        or to increase the range of health point when player level up

        Returns:
            int: health point range
        """

        return self.__health_points_range

    def get_hp(self):
        """return health point

        Returns:
            int: will check if the player is live and will used to decrease 
            the curent health
        """
        return self.__health_points

    def get_dmg(self):
        """return damage point

        Returns:
            int: damge will decrease the health point
        """
        return self.__damage_points

    def get_name(self):
        """return name

        Returns:
            str: name of enitiy
        """
        return self.__name

    def remove_pet_from_pet_list(self, curent_pet):
        """remove the curent pet from pet_list

        Args:
            curent_pet (Pet,EnemyPet): this is the defeated ped
        """
        try:
            self.get_pet_list().remove(curent_pet)
        except ValueError as err:
            print("remove_pet_from_pet_list that pet isn't in the list: ", err)
        except:
            print("Opps ... remove_pet_from_pet_list")

    def add_to_pet_list(self, pet):
        """add pet to the pet_list

        Args:
            pet (Pet,EnemyPet): this is the pet to be added to pet list
        """
        self.__pet_list.append(pet)

    def set_pet_list(self, this_list):
        """set the pet list

        Args:
            this_list (list): set the pet_list used to set default for player
        """
        self.__pet_list = this_list

    def set_name(self, name):
        """set the name

        Args:
            name (str): entity name
        """
        self.__name = str(name)

    def set_level(self, value):
        """set the level

        Args:
            value (int): this is the player leve or pet level

        Raises:
            ValueError:
                if the value isn't int
        """
        try:
            self.__level = int(value)
        except ValueError as err:
            print("set_level must be an integer", err)

    def set_increase(self, value):
        """set the increse

        Args:
            value (int): this is the increase value

        Raises:
            ValueError:
                if the value isn't int
        """
        try:
            self.__INCREASE = int(value)
        except ValueError as err:
            print("set_increase must be an integer", err)

    def set_hp_range(self, value):
        """set  health point range

        Args:
            value (int): this is the increase value

        Raises:
            ValueError:
                if the value isn't int
        """
        try:
            self.__health_points_range = int(value)
        except ValueError as err:
            print("set_hp_range must be an integer", err)

    def set_hp(self, hp):
        """set heal ponts

        Args:
            value (int): this is the increase value

        Raises:
            ValueError:
                if the value isn't int
        """
        try:
            self.__health_points = int(hp)
        except ValueError as err:
            print("set_hp must be an integer", err)

    def set_dmg(self, dmg):
        """set dmg

        Args:
            value (int): this is the increase value

        Raises:
            ValueError:
                if the value isn't int
        """
        try:
            self.__damage_points = int(dmg)
        except ValueError as err:
            print("set_dmg must be an integer", err)

    def __str__(self):
        """return stats win name hp dmg

        Returns:
            str: name hp and damig formate string
        """
        return "name: {},hp: {},dmg: {}".format(self.get_name(), self.get_hp(), self.get_dmg())

    def restore_player_and_pets_hp(self):
        """This function will restor the health and of player and pets if have

        This function will be used if player go in to the castle
        """
        self.set_hp(self.get_hp_range())

        if self.get_pet_list():
            for i in self.get_pet_list():
                i.set_hp(i.get_hp_range())
