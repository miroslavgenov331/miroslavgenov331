from enemy_class import Enemy
"""This is the EnemyPet class

This class will be used to fill the enemy pet list

Class:
    EnemyPet(Enemy)

"""


class EnemyPet(Enemy):
    """This is enemy pet wich will attack player or player pet


    """

    def __init__(self, name, hp, dmg):
        """EnemyPet consturctor

        Args:
            enemy_pet_name (str): name of the enemy pet
            health_points (int): healt
            damage_points (int): damage
        """
        super().__init__(name, hp, dmg)
