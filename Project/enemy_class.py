from entity_class import Entity
"""This is the class Enemy
from entity_class import Entity

Class:
    Entity(Entity)

"""


class Enemy(Entity):
    """This class inherit Entity

    This is the Enemy class and will be used to attack the player

    Atributes:

    Methods:
        def take_dmg(damage_points, my_self):
            when player attack the enemy health_points will be decreased if enemy health_points = 0
            then player will receive bonus 
        def attack(object, damage_points)
            enemy attack the object with the given damage_points if the object is alive

    """

    def __init__(self, name, hp, dmg):
        """Constructor of Enemy

        Args:
            enemy_name (str): this is enemy enemy_name
            health_points (int): this is the enemy health
            damage_points (int): this is enemy damage used to decrease health
        """
        super().__init__(name, hp, dmg)

    def take_dmg(self, dmg, my_self):
        """This function is used to decrease the enemy health 

        This function will decrease the enemy health if the damage_points is greather
        than enemy healp will be set to zero and the player (my_self) 
        or the object who attack the enemy will receive exp_bonus
        else if enemy health_points is greather than the damage_points , enemy health_points will be decreased
        with that damage_points 

        Args:
            damage_points (int): this is the damage_points from the player or from pet if player
                have pet list
            my_self (Player): this is the player who attacks the enemy and
                and will receive bonus if enemy health_points == 0
        """
        if dmg >= self.get_hp():
            self.set_hp(0)
            # my self is the player
            my_self.set_exp_when_k()
        else:
            self.set_hp(self.get_hp()-dmg)
            # need to heal the player
            my_self.set_hp(int(my_self.get_hp()+self.get_hp()//8))

    def attack(self, object, dmg):
        """With this function enemy can attack the given object

        This function will check if object is alive and if alive will take damage_points
        else will do nothing

        Args:
            object (Player,Pet): the object can be player or pet 
            damage_points (int): this value will be pass in object.take_damge(value)
        """
        if object.is_alive():
            object.take_dmg(dmg)
            # print("object is alive")
        # else:
        #     # print("object is dead")
