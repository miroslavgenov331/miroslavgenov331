from pet_class import Pet
from player_class import Player
from enemy_pet_class import EnemyPet
from map_class import Map
from save_class import Save
from menu_class import Menu
from enemy_class import Enemy


class Game:
    """Class that combines all other

    This class runs the game logic.Allow user to move in the map
    save game , see stats , see menu , pick from menu , place castle 
    fight , buy pets , attack , inspect enemy 



    Atributes:   
        __strings_with_dir_to_save_dirs : str
            this file will saves the directory of the save file of the new 
            player and this file saved_savess.txt will be used to check if
            the player enter name that exists  

        __user_input_str : str
            gets the user input from the consol defalt("")

        new_player : Player
            this is the player who is playing the game defalt(Player(""))

        new_map : Map
            this is the game map 

        game_save : Save
            this is used to create game save to the player who is playing 
            the game

        my_menu : Menu
            in different states of the game this shows different menus

        enemy_list: list
            this is the enemyes who player must defeat default(
                Enemy("Enemy1", 125, 15), Enemy("Enemy2", 200, 30)]
            )

    Methods:

        __init__(self):
            This is the constructor

        get_enemy_list(self):
            Return list of enemys

        check_name_space(self, name):
            Check the name if have space

        get_defaults_to_save(self):
            This function will be used to save in a list the default

        set_enemies_in_list_health_to_max(self):
            This function will set the health of all enemy in enemy_list to max

        check_player_name(self, player_name):
            This function will check the name if the name is correct

        clear_enemy_pet_list_and_set_defalt_pet_list(self):
            [clear_enemy_pet_list_and_set_defalt_pet_list]

        set_new_game_defaults_and_clear_objects(self, player_name):
            This function will be used to set the new game object atributes

        new_game(self):
            This is the new_game function where player start new game

        user_input(self, message):
            This function will ask user to enter input from console

        get_user_input(self):
            Return the user input from the consol

        set_user_input(self, message):
            This function set user input primery to be ("") or ("0")

        game(self):
            This functio will run the game and print menu to pick

        load_game(self):
            This function will load the game and load the saved data to player

        load_enemys_pets(self):
            This function will load enemy with some pet

        fight(self):
            This is the game fight 

        play(self):
            this function plays the game

        castle_shop(self):
            This function is the caslt show when player enter the castle

        castle_shop_logic(self, user_input):
            This function is castle_shop_logic

        pet_level_up_shop(self):
            This function will print pet_list

        pet_level_up_shop_logic(self, user_input):
            This function will upgrade the with the given user_input

        show_map(self):
            This functions print the map

    """

    # this is the directory where player directory saves are stored
    __strings_with_dir_to_save_dirs = "Project/Save_games/saved_saves.txt"

    def __init__(self):
        """This is the constructor

        Raises:
            FileExistsError
                if this file saved_saves.txt exist
        """
        try:
            open(self.__strings_with_dir_to_save_dirs, 'x')
        except FileExistsError:
            pass

        self.__user_input_str = str("")
        self.__new_player = Player("")
        self.__new_map = Map()
        self.__game_save = Save()
        self.__my_menu = Menu()
        self.__enemy_list = [
            Enemy("Enemy1", 400, 35), Enemy("Enemy2", 3416, 115)]

    def get_enemy_list(self):
        """Return list of enemys

        Returns:
            list: this is the list of enemyes
        """
        return self.__enemy_list

    def check_name_space(self, name):
        """Check the name if have space

        Have for loop in name and if have space return false else if there is 
        no False will return True

        Args:
            name (str): this is the given name from user input

        Returns:
            bool: if the name have space will return False else will return True
        """
        for i in name:
            if i == " ":
                return False
        return True

    def get_defaults_to_save(self):
        """This function will be used to save in a list the default

        This function will be used to save in a list the default values of
        player and map,pet list when player start new_game. This data in 
        the list will be used to be written in the player save

        Returns:
            list: this is the list which contains the player default data
        """
        default_list = []

        default_list.append(self.__new_player.get_name())
        default_list.append(self.__new_player.get_hp())
        default_list.append(self.__new_player.get_hp_range())
        default_list.append(self.__new_player.get_dmg())
        default_list.append(self.__new_player.get_reward())
        default_list.append(self.__new_player.get_exp())
        default_list.append(self.__new_player.get_exp_range())
        default_list.append(self.__new_player.get_gold())
        default_list.append(self.__new_player.get_gold_reward())
        default_list.append(self.__new_player.get_level())
        default_list.append(self.__new_player.get_win())
        # map
        default_list.append(self.__new_map.get_player_position())
        default_list.append(self.__new_map.get_map_to_string())
        default_list.append(self.__new_map.is_castle_there())

        # this list must to be last becouse its dinamic
        default_list.append(self.__new_player.get_pet_list())

        return default_list

    def set_enemies_in_list_health_to_max(self):
        """This function will set the health of all enemy in enemy_list to max


        This function will used in new_game to set the health of all enemy in 
        enemy_list to max 
        """
        for i in self.get_enemy_list():
            i.set_hp(i.get_hp_range())

    def check_player_name(self, player_name):
        """This function will check the name if the name is correct

        This function will check the name if the name is correct to be 
        added in the saves

        Args:
            player_name (str): this is the player name from the get_user_input

        Returns:
            bool: if return True the name is correct if False incorect
        """

        name_corect = False
        name_exist = True

        if self.check_name_space(player_name) == True:
            name_corect = True
        else:
            print("\nIncorect name  !!!")
            name_corect = False

        if name_corect == True and self.__game_save.is_created(
                player_name) == False:
            name_exist = False
        if name_corect == True and self.__game_save.is_created(
                player_name) == True:
            print("\nName exixsts !!!")
            name_exist = True

        if name_corect == True and name_exist == False:
            return True
        else:
            return False

    def clear_enemy_pet_list_and_set_defalt_pet_list(self):
        """[clear_enemy_pet_list_and_set_defalt_pet_list]
        """
        for i in self.get_enemy_list():
            i.set_pet_list(list())
        self.load_enemys_pets()

    def set_new_game_defaults_and_clear_objects(self, player_name):
        """This function will be used to set the new game object atributes

        This function will be used to set the new game object atributes and
        from the previous new game some data

        Args:
            player_name (str): the name of the player
        """
        self.__new_player = Player(player_name)
        self.__new_map = Map()
        self.__my_menu = Menu()

        self.set_enemies_in_list_health_to_max()
        self.clear_enemy_pet_list_and_set_defalt_pet_list()

    def new_game(self):
        """This is the new_game function where player start new game

        This function will set player , map , values to default and check if
        the given name is correct
        """

        default_save_list = self.get_defaults_to_save()

        self.load_enemys_pets()

        # create game save
        self.__game_save.create_save()
        # set the save data
        self.__game_save.set_save_data(default_save_list)

        # if the player defeat all enemys player win else try again
        if self.play():
            print("you win the game !!!")
            self.set_new_game_defaults_and_clear_objects("")
        else:
            if self.__new_player.get_hp() == 0:
                self.set_new_game_defaults_and_clear_objects("")
                print("try again !!")
            self.set_user_input("")
        # start the game

    def user_input(self, message):
        """This function will ask user to enter input from console

        Args:
            message (str): this is the given message to ask user to input
        """
        self.__user_input_str = input(message)

    def get_user_input(self):
        """Return the user input from the consol

        Returns:
            str: this is the user input from the consol
        """
        return self.__user_input_str

    def set_user_input(self, message):
        """This function set user input primery to be ("") or ("0")

        Args:
            message (str): desired input to be set
        """
        self.__user_input_str = message

    def game(self):
        """This functio will run the game and print menu to pick

        This functio will run the game and print menu to pick new game,
        exit , load game
        """
        print("Welcome to my Heros")

        while not self.get_user_input() == '0':

            # show game menu
            self.__my_menu.show_menu(self.__my_menu.get_game_menu())

            # get user input
            self.user_input("Enter number from menu: ")

            if self.get_user_input() == '1':
                self.set_user_input("")

                self.user_input("Enter your name: ")

                while self.check_player_name(
                        self.get_user_input()
                ) == False or self.get_user_input() == '0':

                    self.user_input(
                        "Your name is incorect enter corect name: ")
                self.set_new_game_defaults_and_clear_objects(
                    self.get_user_input())
                self.new_game()
                self.set_new_game_defaults_and_clear_objects("")
            elif self.get_user_input() == '2':
                self.set_new_game_defaults_and_clear_objects("")
                self.set_user_input("")
                self.load_game()
                self.set_new_game_defaults_and_clear_objects("")

    def load_game(self):
        """This function will load the game and load the saved data to player

        Raisese:
            ValueError:
                if user dont enter int
        """

        game_save = self.__game_save
        new_player = self.__new_player
        new_map = self.__new_map

        if game_save.check_for_saves():
            game_save.show_saves_from_file()

            # self.user_input_str = ""
            # self.set_user_input("")
            while self.get_user_input() != "0":

                # this is the menu
                self.__my_menu.show_menu(self.__my_menu.get_load_game_menu())
                self.user_input("Pick from menu: ")

                if self.get_user_input() == '1':
                    # TODO :  need to change this when add more atributes
                    # to player class
                    self.user_input("Your index: ")
                    try:
                        # With this block player can continue the game where
                        # they saves the game

                        loaded_dada_list = []

                        if game_save.get_player_by_index_data_list(
                                int(self.get_user_input())) != False:
                            # If player save's exists this block will load
                            #   the saved data

                            this_data = game_save.get_player_by_index_data_list(
                                int(self.get_user_input()))

                            loaded_dada_list = this_data
                            # TODO : WHEN LOAD GAME NEED TO CLEAR THE PREVIOUS LIST OF PETS
                            # self.set_new_game_defaults_and_clear_objects(
                            #     loaded_dada_list[0][0:-1]
                            # )

                            # this will load the player data
                            new_player.set_name(loaded_dada_list[0][0:-1])
                            new_player.set_hp(loaded_dada_list[1][0:-1])
                            new_player.set_hp_range(loaded_dada_list[2][0:-1])
                            new_player.set_dmg(loaded_dada_list[3][0:-1])
                            new_player.set_reward(loaded_dada_list[4][0:-1])
                            new_player.set_exp(loaded_dada_list[5][0:-1])
                            new_player.set_exp_range(loaded_dada_list[6][0:-1])
                            new_player.set_gold(loaded_dada_list[7][0:-1])
                            new_player.set_gold_reward(
                                loaded_dada_list[8][0:-1])
                            new_player.set_level(loaded_dada_list[9][0:-1])
                            new_player.set_win(loaded_dada_list[10][0:-1])

                            # map
                            new_map.set_player_position(
                                loaded_dada_list[11][0:-1])
                            new_map.set_map(new_map.get_map_string_to_list(
                                loaded_dada_list[12][0:-1]))

                            # is castle there is false will set to false else true
                            if loaded_dada_list[13][0:-1] == "False":
                                new_map.set_castle_there(False)
                            else:
                                new_map.set_castle_there(True)

                            # this is for the pet position in the name_+saves.txt
                            pet_position = 14
                            # this is the number of Pet atributes
                            pet_position_range = 4

                            # if pet position in seved_seves.txt is 14 and there
                            # is [] this mean player pet list in empty
                            if loaded_dada_list[pet_position][
                                    0:-1] == "[]" and len(
                                    loaded_dada_list) == pet_position+1:
                                new_player.set_pet_list(list())
                            else:
                                # else the data len is greather than 14 then there is pet data
                                # if there is more than one Pet jump with 4 position to get the next pet
                                # clear the player pet list
                                # self.__new_player.set_pet_list(list())
                                for i in range(pet_position, len(
                                        loaded_dada_list), pet_position_range):

                                    saved_pet = Pet(loaded_dada_list[i][0:-1],
                                                    loaded_dada_list[i+1][0:-1],
                                                    loaded_dada_list[i+2][0:-1])
                                    new_player.add_to_pet_list(
                                        saved_pet)

                            # get the wanted save dir  and  make it as my own
                            game_save.set_save_name_dir(
                                game_save.get_player_dir_by_index(
                                    int(self.get_user_input()))[0:-1])
                            self.set_user_input("")
                            if self.play():
                                print("you win")
                            else:
                                if not new_player.is_alive():
                                    print("try again")

                    except ValueError:
                        print("\nEnter only numbers !!!\n")
                    # self.user_input_str = ""
                    self.set_user_input("")

                elif self.get_user_input() == '2':

                    # self.user_input_str = input(str("Your index: "))
                    self.user_input("Your index: ")

                    try:
                        game_save.remove_player_save(
                            int(self.get_user_input()))
                    except ValueError:
                        print("\nEnter only numbers !!!\n")

                self.set_user_input("0")
            # self.user_input_str = ""
            self.set_user_input("")
        else:
            # if there is no saves
            print("\nEmpty save !!")

    def load_enemys_pets(self):
        """This function will load enemy with some pet
        """
        self.get_enemy_list()[0].add_to_pet_list(EnemyPet(
            "Enemy1Pet1", 5, 2))
        self.get_enemy_list()[0].add_to_pet_list(EnemyPet(
            "Enemy1Pet1", 10, 2))
        self.get_enemy_list()[0].add_to_pet_list(EnemyPet(
            "Enemy1Pet1", 22, 2))
        self.get_enemy_list()[0].add_to_pet_list(EnemyPet(
            "Enemy1Pet2", 23, 3))
        self.get_enemy_list()[0].add_to_pet_list(EnemyPet(
            "Enemy1Pet3", 24, 4))

        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 500, 100))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 350, 105))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 750, 109))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 900, 120))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 750, 130))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 1200, 325))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 1450, 325))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 2450, 325))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 3450, 325))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 4450, 325))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 5450, 325))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 6450, 325))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 8500, 500))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 8500, 500))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 9900, 1000))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 10500, 1250))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 12000, 1200))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 13500, 1500))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 15500, 2500))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 28500, 3500))

        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 8116, 3500))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 325, 120))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 250, 100))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 150, 20))
        self.get_enemy_list()[1].add_to_pet_list(
            EnemyPet("Enemy2Pet1", 250, 100))

    def fight(self):
        """This is the game fight 

        In enemy list first enemy will fight.In pet list first pet will fight.
        If pet is defeated will remove the defeated pet if no pet left will atack 
        player .If no enemy pet list player pet will atack enemy. 

        Returns:
            bool : If player defeat true
                False if player is defeated
        """

        current_player = self.__new_player
        current_enemy = self.get_enemy_list()[current_player.get_win()]

        self.clear_enemy_pet_list_and_set_defalt_pet_list()

        while current_player.is_alive():

            print()

            if current_enemy.is_alive():

                if current_enemy.get_pet_list():

                    if current_player.get_pet_list():

                        current_player.show_name_hp_dmg()
                        current_player.show_pets_for_fight()

                        print()
                        current_enemy.show_pets()
                        current_enemy.show_name_hp_dmg()
                        print()
                        input("Enter to continue the fight")

                        current_player.attack(
                            current_enemy.get_pet_list()[0],
                            current_player.get_pet_list()[0].get_dmg())

                        if current_enemy.get_pet_list()[0].is_alive():
                            current_enemy.get_pet_list()[0].attack(
                                current_player.get_pet_list()[
                                    0], current_enemy.get_pet_list()[0].get_dmg())

                            if current_player.get_pet_list()[0].is_alive() == False:
                                current_player.remove_pet_from_pet_list(
                                    current_player.get_pet_list()[0])

                        else:
                            current_enemy.remove_pet_from_pet_list(
                                current_enemy.get_pet_list()[0])

                    else:

                        print()
                        current_player.show_name_hp_dmg()

                        print()
                        current_enemy.show_pets()
                        current_enemy.show_name_hp_dmg()
                        print()
                        input("Enter to continue the fight")

                        current_player.attack(current_enemy.get_pet_list()[
                                              0], current_player.get_dmg())

                        if current_enemy.get_pet_list()[0].is_alive():

                            current_enemy.get_pet_list()[0].attack(
                                current_player, current_enemy.get_pet_list()[
                                    0].get_dmg())
                        else:
                            current_enemy.remove_pet_from_pet_list(
                                current_enemy.get_pet_list()[0])
                else:

                    if current_player.get_pet_list():

                        current_player.show_name_hp_dmg()
                        current_player.show_pets_for_fight()
                        print()
                        current_enemy.show_name_hp_dmg()
                        print()
                        input("Enter to continute the fight")

                        current_player.attack(
                            current_enemy, current_player.get_pet_list()[
                                0].get_dmg())

                        if current_enemy.is_alive():
                            current_enemy.attack(current_player.get_pet_list()[
                                                 0], current_enemy.get_dmg())
                            if current_player.get_pet_list()[
                                    0].is_alive() == False:
                                current_player.remove_pet_from_pet_list(
                                    current_player.get_pet_list()[0])

                        else:
                            return 1

                    else:
                        # print("\tplayer dont have pet")
                        current_player.show_name_hp_dmg()
                        print()
                        current_enemy.show_name_hp_dmg()
                        print()
                        input("Enter to continue the fight")

                        current_player.attack(
                            current_enemy, current_player.get_dmg())

                        if current_enemy.is_alive() == False:
                            # print("Enemy is dead")
                            return 1
                        else:
                            current_enemy.attack(
                                current_player, current_enemy.get_dmg())
        # print("Player is dead")
        return 0

    def play(self):
        """this function plays the game

        The first while loop shows the map show the menu to move left to right ,
            to save , to show player stats , to place castle ,exit

        if on next block have enemy will ask to watch the fight

        Raises:
            IndexError:
                if the picked player pet is not in range of the indexes
        Returns:
            bool: if player defeat all anemy will return 1
                if the player is defeat will return 0
        """

        while self.get_user_input() != '0':

            self.show_map()

            # self.show_play_menu()
            self.__my_menu.show_play_menu(
                self.__my_menu.get_playe_menu(),
                self.__new_map.is_castle_there())

            self.user_input("\nPick from menu: ")
            print()

            if self.get_user_input() == '1':

                move_left = self.__new_map.move_left()

                if move_left == self.__new_map.get_enemy_here():
                    print("Do you want to attack!!!!")

                    self.set_user_input("")
                    while not self.get_user_input() == "0":

                        self.__my_menu.show_menu(
                            self.__my_menu.get_map_attack_menu())
                        self.user_input("Pick: ")

                        if self.get_user_input() == '1':
                            if self.fight():
                                self.__new_player.set_win(
                                    self.__new_player.get_win()+1)
                                # self.set_win(self.get_win()+1)
                                self.__new_map.clear_map_position(
                                    self.__new_map.get_player_position_from_map()-1)
                                # print("you win")
                                if len(self.get_enemy_list()
                                       ) == self.__new_player.get_win():
                                    return 1
                            else:
                                # print("you lose")
                                return 0
                        self.set_user_input("0")
                    self.set_user_input("")

                elif move_left == self.__new_map.get_welcome_castle():
                    print("welcome to castle")

                    self.__new_player.restore_player_and_pets_hp()

                    self.set_user_input("")

                    while not self.get_user_input() == '0':
                        self.castle_shop()
                    self.set_user_input("")

                elif move_left == self.__new_map.get_exit_castle():
                    print("you exit castle")
                else:
                    print("no enemy")

            elif self.get_user_input() == '2':
                move_right = self.__new_map.move_right()

                if move_right == self.__new_map.get_enemy_here():
                    print("Do you want to attack!!!!")

                    self.set_user_input("")
                    while not self.get_user_input() == "0":

                        self.__my_menu.show_menu(
                            self.__my_menu.get_map_attack_menu())
                        self.user_input("Pick: ")

                        if self.get_user_input() == '1':
                            if self.fight():
                                self.__new_player.set_win(
                                    self.__new_player.get_win()+1)
                                # self.set_win(self.get_win()+1)
                                self.__new_map.clear_map_position(
                                    self.__new_map.get_player_position()+1)
                                if len(self.get_enemy_list()
                                       ) == self.__new_player.get_win():
                                    # print("you win")
                                    return 1
                            else:
                                # print("you lose")
                                return 0
                        self.set_user_input("0")
                    self.set_user_input("")

                elif move_right == self.__new_map.get_welcome_castle():
                    print("welcome to castle")

                    self.__new_player.restore_player_and_pets_hp()

                    self.set_user_input("")

                    while not self.get_user_input() == '0':
                        self.castle_shop()
                    self.set_user_input("")

                elif move_right == self.__new_map.get_exit_castle():
                    print("you exit castle")
                else:
                    print("no enemy")

            elif self.get_user_input() == '3':
                # This block will save the player data when press 3
                # When player press 3 the player data like name , map ,
                # castle , etc will be stored in file

                player_data = []

                # Player data
                player_data.append(self.__new_player.get_name())
                player_data.append(self.__new_player.get_hp())
                player_data.append(self.__new_player.get_hp_range())
                player_data.append(self.__new_player.get_dmg())
                player_data.append(self.__new_player.get_reward())
                player_data.append(self.__new_player.get_exp())
                player_data.append(self.__new_player.get_exp_range())
                player_data.append(self.__new_player.get_gold())
                player_data.append(self.__new_player.get_gold_reward())
                player_data.append(self.__new_player.get_level())
                player_data.append(self.__new_player.get_win())

                # Map data
                player_data.append(
                    self.__new_map.get_player_position())
                player_data.append(self.__new_map.get_map_to_string())
                player_data.append(self.__new_map.is_castle_there())

                # Pet

                if self.__new_player.get_pet_list():
                    for i in self.__new_player.get_pet_list():
                        player_data.append(i.get_name())
                        player_data.append(i.get_hp())
                        player_data.append(i.get_dmg())
                        player_data.append(i.get_level())
                else:
                    player_data.append(self.__new_player.get_pet_list())

                self.__game_save.set_save_data(player_data)
            elif self.get_user_input() == "4":
                print(self.__new_player)
                if self.__new_player.get_pet_list():
                    self.__new_player.show_pets()
            elif self.get_user_input(

            ) == '5' and not self.__new_map.is_castle_there():
                self.__new_map.set_castle_there(True)
                self.__new_map.place_castle()
                print("You created your castle")

                self.__new_player.restore_player_and_pets_hp()

                self.set_user_input("")

                while not self.get_user_input() == '0':
                    self.castle_shop()
                self.set_user_input("")

        self.set_user_input("")

    def castle_shop(self):
        """This function is the caslt show when player enter the castle

        This function is the caslt show when player enter the castle
        this function will call the casle_show_login  and if the castle
        show_logick return 0 castle_shop will set_user_input to "0"

        After your of castle_show() you must set self.__user_input to ""


        """
        print("Your gold: ", self.__new_player.get_gold())
        self.__my_menu.show_castle_menu(
            self.__my_menu.get_castle_menu(), self.__new_player.get_pet_list())
        self.user_input("Enter from menu: ")
        if self.castle_shop_logic(self.get_user_input()) == False:
            self.set_user_input("0")

    def castle_shop_logic(self, user_input):
        """This function is castle_shop_logic

        This function is castle_shop_logic where the player if dont have pet
        cant buy and if have pet can upgrade
        If player have pat then if he wants to level upd the pet
        this function will call pet_level_up_shop and if return 0 that mean the 
        player dont want to level up an will set user_input to "0"
        Then if the user input i 0 this function return 0 

        Args:
            user_input (str): keyboard input

        Returns:
            bool: if the user input is '0' then return 0 
        """
        if user_input == '1':
            if self.__new_player.get_gold():
                if self.__new_player.get_gold()-self.__my_menu.get_cost() >= 0:
                    self.__new_player.set_gold(
                        self.__new_player.get_gold()-self.__my_menu.get_cost())
                    self.__new_player.add_to_pet_list(
                        Pet("Bo", 5, 2))
            else:
                print("not enougf money")
        elif user_input == '2' and self.__new_player.get_pet_list():
            self.set_user_input("")
            while user_input != "0":
                if self.pet_level_up_shop() == 0:
                    user_input = "0"
        elif user_input == '0':
            return 0

    def pet_level_up_shop(self):
        """This function will print pet_list

            If self.__user_input is not "0"
            then will call pet_level_up_shop_logic

        Returns:
            0: if user input is "0"
        """
        print("Your gold: ", self.__new_player.get_gold())
        self.__new_player.show_pets()
        print("0.Exit\n")
        self.user_input("Pet number to level up: ")
        if not self.get_user_input() == "0":
            self.pet_level_up_shop_logic(self.get_user_input())
        else:
            self.set_user_input("")
            return 0

    def pet_level_up_shop_logic(self, user_input):
        """This function will upgrade the with the given user_input

        This function will upgrade the with the given user_input this
        is the index of the pet decresed by 1 and will try if the gold 
        is True then will check if gold - cost is eq or great then will set the
        player gold and level up the pet

        Args:
            user_input (str): keyboard input


        Raises:
            IndexError:
                if the given user_input is out of range of the pet list
        """
        try:
            if self.__new_player.get_gold():
                if self.__new_player.get_gold()-self.__my_menu.get_cost() >= 0:
                    self.__new_player.get_pet_list()[int(
                        user_input)-1].level_up()
                    self.__new_player.set_gold(
                        self.__new_player.get_gold()-self.__my_menu.get_cost())
        except IndexError:
            pass
        except:
            pass

    def show_map(self):
        """This functions print the map
        """
        print(self.__new_map.get_map())


game = Game()
game.game()
