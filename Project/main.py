from game_class import Game
""" This file is to run the game and hame 

Functions:
    main()
        run the game
    
Block:
    if __name__=='__main__':
        main()
"""


def main():
    """This function run the game
    """
    new_game = Game()
    new_game.game()


if __name__ == '__main__':
    main()
