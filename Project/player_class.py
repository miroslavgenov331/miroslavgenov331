from entity_class import Entity
from pet_class import Pet
from error_class import NotAPetType


class Player(Entity):
    """This is the Player class inherit Enety class

    Atributes:

        exp(int): this is the player exp default(0)

        exp_range(int): this is the player exp_range default(20)

        reward(int): this is the player reward default(10)

        gold(int): this is the player gold default(500)

        gold_reward(int): this is the player gold_reward default(500)

        win(int): this is the player win default(0)

    Methods:

        __init__(name):
            Constructor have default values and calls Entity

        get_win():
            return win

        __str__():
            return string with all atributes

        level_up():
             This function will increase the player atributes

        get_gold():
            return gold

        get_gold_reward():
            return gold_reward

        get_exp():
            return exp

        get_exp_range():
            return exp_range

        get_reward():
            return reward

        set_gold(value):
            this function will set set_gold but the value must be integer

        set_win(value):
            this function will set set_win but the value must be integer

        set_gold_reward(value):
            this function will set set_gold_reward but the value must be 
            integer

        set_exp(value):
            this function will set set_exp but the value must be integer

        set_exp_range(value):
            this function will set set_exp_range but the value must be integer

        set_reward(value):
            this function will set set_reward but the value must be integer

        set_exp_when_k():
            This function will set the exp when player defeat enemy or enemy 
            pet


    """

    __exp = int()
    __exp_range = int()
    __reward = int()
    __gold = int()
    __gold_reward = int()
    __win = int()

    def __init__(self, name):
        """Constructor have default values and calls Entity

        super().__init__(name, hp=100, dmg=15)

        self.__exp = 0
        self.__exp_range = 20
        self.__reward = 10
        self.__gold = 500
        self.__gold_reward = 500
        self.__win = 0

        Args:
            name (str): name of the player
        """
        super().__init__(name, health_points=100, damage_points=22)

        self.set_exp(0)
        self.set_exp_range(20)
        self.set_reward(10)
        self.set_gold(500)
        self.set_gold_reward(500)
        self.set_win(0)

    def add_to_pet_list(self, pet):

        try:
            if isinstance(pet, Pet):
                super().add_to_pet_list(pet)
            else:
                raise NotAPetType("This is not a pet")
        except NotAPetType as err:
            print("In add_to_pet_list error: ", err)

    def get_win(self):
        """return win

        Returns:
            int: this is the win of the player
        """
        return self.__win

    def __str__(self):
        """return string with all atributes

        Returns:
            str: formated string
        """
        return "name: {},hp: {},dmg: {},exp: {},exp_range: {},reward: {},increase: {},level: {},gold:{},wins: {}".format(
            super().get_name(), super().get_hp(), super().get_dmg(),
            self.get_exp(), self.get_exp_range(), self.get_reward(),
            super().get_increase(), super().get_level(), self.get_gold(), self.get_win()
        )

    def level_up(self):
        """ This function will increase the player atributes

        This function will increase the player atributes hp , dmg
        exp , exp range , hp range , gold
        """
        new_hp = int(super().get_hp_range() * super().get_increase())
        super().set_hp(new_hp)
        super().set_hp_range(new_hp)
        dmg = int(super().get_dmg()*super().get_increase())
        super().set_dmg(dmg)

        self.set_exp_range(int(self.get_exp_range()*self.get_increase()))
        self.set_reward(int(self.get_reward()*self.get_increase()))

        super().set_level(super().get_level()+1)

        self.set_gold_reward(
            int(self.get_gold_reward()*super().get_increase()))
        self.set_gold(self.get_gold_reward())

        print("!!! WOW YOU LEVE UP !!!")

    def get_gold(self):
        """return gold

        Returns:
            int: return the player gold
        """
        return self.__gold

    def get_gold_reward(self):
        """return gold_reward

        Returns:
            int: the gold reward
        """
        return self.__gold_reward

    def get_exp(self):
        """return exp

        Returns:
            int: the exp of player
        """
        return self.__exp

    def get_exp_range(self):
        """return exp_range

        Returns:
            int: the exp_range of player
        """
        return self.__exp_range

    def get_reward(self):
        """return reward

        Returns:
            int: the reward of player
        """
        return self.__reward

    def set_gold(self, value):
        """this function will set set_gold but the value must be integer

        Args:
            value (int): this is the given value

        Raises:
            ValueError:
                if the value isn't int
        """
        try:
            self.__gold = int(value)
        except ValueError as err:
            print("Player set_gold value must be an integer", err)
        except:
            print("Unexpected error")

    def set_win(self, value):
        """this function will set set_win but the value must be integer

        Args:
            value (int): this is the given value

        Raises:
            ValueError:
                if the value isn't int
        """
        try:
            self.__win = int(value)
        except ValueError as err:
            print("Player set_win value must be an integer", err)
        except:
            print("Unexpected error")

    def set_gold_reward(self, value):
        """this function will set set_gold_reward but the value must be integer

        Args:
            value (int): this is the given value

        Raises:
            ValueError:
                if the value isn't int
        """
        try:
            self.__gold_reward = int(value)
        except ValueError as err:
            print("Player set_gold_reward value must be an integer", err)
        except:
            print("Unexpected error")

    def set_exp(self, value):
        """this function will set set_exp but the value must be integer

        Args:
            value (int): this is the given value

        Raises:
            ValueError:
                if the value isn't int
        """
        try:
            self.__exp = int(value)
        except ValueError as err:
            print("Player set_exp value must be an integer\n", err)
        except:
            print("Unexpected error")

    def set_exp_range(self, value):
        """this function will set set_exp_range but the value must be integer

        Args:
            value (int): this is the given value

        Raises:
            ValueError:
                if the value isn't int
        """
        try:
            self.__exp_range = int(value)
        except ValueError as err:
            print("Player set_exp_range value must be an integer\n", err)
        except:
            print("Unexpected error")

    def set_reward(self, value):
        """this function will set set_reward but the value must be integer

        Args:
            value (int): this is the given value

        Raises:
            ValueError:
                if the value isn't int
        """
        try:
            self.__reward = int(value)
        except ValueError as err:
            print("Player set_reward value must be an integer\n", err)
        except:
            print("Unexpected error")

    def set_exp_when_k(self):
        """This function will set the exp when player defeat enemy or enemy pet
        """
        if self.get_exp()+self.get_reward() >= self.get_exp_range():
            self.__exp = self.get_exp()+self.get_reward()-self.get_exp_range()
            self.level_up()

        else:
            self.__exp = int(self.get_exp()+self.get_reward())
            self.set_gold(int(self.get_gold()+self.get_reward()))
