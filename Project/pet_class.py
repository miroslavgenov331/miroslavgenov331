from entity_class import Entity
"""This file contains Pet class

Class:
    Pet(Entity)
        this is the Pet class wich will fill the player pet list
"""


class Pet(Entity):

    """This is the Pet class which inherit Entity

    This class will be used to fill the player pet list and attack enemy or enemy pet.
    If player want will level up the pet 

    Methods:
        def __init__(name, hp, dmg):
            consturtor
        def __str__():
            will use to print the info about pet
        def level_up():
            this will increase the pet statistic and level up the pet
            will add dmg , hp and lvl
    """

    def __init__(self, name, hp, dmg):
        """This is the constructor

        This function call super().__init__(name,health,dmg) from Entity

        Args:
            name (str): name of the pet
            hp (int): health points
            dmg (in): damage
        """
        super().__init__(name, hp, dmg)

    def __str__(self):
        """This function dunder method and return pet str info

        Returns:
            str: return name , dmg , and level of the pet
        """
        return "name:{},hp:{},dmg:{},level:{}".format(
            super().get_name(), super().get_hp(), super().get_dmg(), super().get_level()
        )

    def level_up(self):
        """This function will level up the pet

        This function will increase heal , dmg , and the level of the pet
        """
        new_hp = int(super().get_hp_range()*super().get_increase())
        super().set_hp(new_hp)
        super().set_hp_range(super().get_hp())

        dmg = int(super().get_dmg()*super().get_increase())

        super().set_dmg(dmg)
        super().set_level(super().get_level()+1)
