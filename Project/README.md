# My Game

How to run the game 
download the Project. Move it to a new folder . Open that new folder
with visual studio code.

Otherwise you will need to change this :
*self.__strings_with_dir_to_save_dirs* in *game_class.py*
and *self.__save_dir_name* in *save_class.py*

#### Game
move left/right , build castle , buy pet , level up pet , watch fights

#### Fight
Enemy have pet list but player dont have pet list .
Then player will atack the first pet in enemy pet list if dead else pet will attack player until player is dead or enemy pet .
If the first enemy pet is dead the pet will be removed from the list and the pet will atack the next pet.
If enemy pet is empty the player will atack the enemy and if enemy is alive the enemy will atack the player until player win or die.
This also aplies for the enemy if the enemy dont have pet list but player have pet list.

If both have pet list each one of the first pet will atack if the other is alive the other will atack if the pet or the other pet is dead , the dead pet will be removed and so on if the pet list of the enemy is empty but the player pet list is't empty the first pet will attack the enemy then if enemy is alive will attack the first player pet if the player pet is dead will be removed .
If the player pet list is empty the enemy will attack the player then the player will attack the enemy.


#### Casle
your castle can heal you and your pet list to max health . In your castle you can buy pet and level up each one of them

---

### Files

1. **Save_games** - folder where when you choose new_game your nick name will be saved in folder whith your nick name +_saves.txt and the saved_saves.txt will be created if you don't have it already and saved_saves.txt will save the directory to your nick name+_saves.txt

2. **name+_saves**.txt - when you choose new game , in Save_games will be created file name+_saves.txt which contains the default player atributes and pet atributes . But in saved_saves.txt will be written your directory to name+_saves.txt

3. **enemy_class.py** - inherit class Entity .Enemy class will be used to fight the player

4. **enemy_pet_class -** inherit class Enemy . In Entity class there is pet_list and enemy pets will be added in that list

5. **entity_class.py** - this is the Parent class of Enemy , Pet , Player .

6. **game_class.py** - combines the game logic. Mix those classes: Pet , Player , EnemyPet , Map, Save, Enemy to make the game playe

5. #### main.py - use to run the game

7. **map_class.py** - this Class is the map where player can move 

8. **menu_class.py** - Have different menus to print and price for pet
9. **pet_class.py** - This inherit Entity this class will be used like EnemyPet , for creating the Player pet_list
10. **plaer_class.py** - This is the Player class and inherit Entity.
11. **save_class.py** - Is used to create save files ,  save data .