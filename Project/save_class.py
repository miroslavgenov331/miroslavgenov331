import os
"""This is the save_clsss which saves data

This class will save data and make files to saves the data and will have file
which will saves the dir of the files wich contains player data

Class:
    Save

"""


class Save:
    """This is the save_clsss which saves data

    This class will save data and make files to saves the data and will have file
    which will saves the dir of the files wich contains player data 

    Atributes:
        SAVE_NAME(str) :
            this will be the current player save dir

        SAVE_DIR_NAME(str):
            this is the directory where player name+_saves.txt will be saved 
            default("Project/Save_games/")

        SAVE_TYPE_NAME(str):
            with this the file of the player save will be created with file type
            by default("_saves.txt") is txt

        saved_name(str):
            this is the name of the file which will save the directory of the
            directory of player saves default("saved")


    Methods:
        def check_for_saves():
            This functin will check if saved_saves.txt is empty or not empty


        def show_saves_from_file():
            This functio will show all player saves

        def remove_player_save(index):
            This function will remove player save

        def add_save_name_in_file():
            This function add the new save dir in saved_saves.txt

        def set_save_data(player_data):
            This function will write in name+_saves.txt and write the data

        def set_save_name_dir(save_name):
            This function set the curent self.__save_name to the given path

        def get_save_name_dir():
            his function return the curent  name+_saves.txt dir

        def is_created(name):
            This function check if the given name have name+_saves.txt

        def get_player_dir_by_index(index):
            This function will get the player dir by index

        def get_player_by_index_data_list(index):
            This function will get the player data with the given index

        def create_save():
            This function create the player name+_saves.txt file 

    """

    def __init__(self):
        """This is the constructor 

        save_name(str) :
            this will be the curent player save dir

        save_dir_name(str):
            this is the directory where player name+_saves.txt will be saved 
            default("Project/Save_games/")

        save_type_Name(str):
            with this the file of the player save will be created with file type
            by default("_saves.txt") is txt

        saved_name(str):
            this is the name of the file which will save the directory of the
            directory of player saves default("saved")


        """
        self.__save_name = ""
        self.__SAVE_DIR_NAME = "Project/Save_games/"
        self.__SAVE_TYPE_NAME = "_saves.txt"
        self.__SAVED_NAME = "saved"

    def check_for_saves(self):
        """This functin will check if saved_saves.txt is empty or not empty

        This function will check the file saved_saves.txt which contains
        strings wich directory of all name_saves.txt when player choose
        to playe new game or save the game

        Returns:
            bool: if saved_saves.txt is empty will return True if not empty 
            will return False 
        """
        with open(self.__SAVE_DIR_NAME+self.__SAVED_NAME+self.__SAVE_TYPE_NAME) as save:
            if not save.read():
                return False
            else:
                return True

    def show_saves_from_file(self):
        """This functio will show all player saves

        This function read from saved_saves.txt and 
        prints the saves but only number of the save and the player name in
        increasing order
        """

        # count the saves
        count_index = 0

        # open seved_saves.txt and read
        with open(self.__SAVE_DIR_NAME+self.__SAVED_NAME+self.__SAVE_TYPE_NAME, 'r') as saves:

            line = saves.readline()
            while line != '':

                # print("this is lenline= ", len(line))
                print("[{}]".format(count_index), end="")

                for i, v in enumerate(line):
                    # this if will subtract the 'Project/Save_games/' and
                    # '_saves.txt' and will print the player name
                    if i >= len(self.__SAVE_DIR_NAME) and i < len(line)-len(self.__SAVE_TYPE_NAME)-1:
                        print(v, end="")
                count_index += 1
                print()
                line = saves.readline()

    def remove_player_save(self, index):
        """This function will remove player save

        This function will remove player save directory from saved_saves.txt 
        and remove the name+_save.txt  file the index is from  show_saves_from_file
        where the user can pick save to remove .

        Will have save_list =list() which will save the saves different than the 
        given index and this save_list , the saved_saves.txt will be rewriten 
        without the save with the given index

        Args:
            index (int): this is the number of the save
        """

        count = 0

        save_list = []

        with open(self.__SAVE_DIR_NAME+self.__SAVED_NAME+self.__SAVE_TYPE_NAME, 'r') as read:
            line = read.readline()

            while line != '':
                if count != index:
                    # save other saves
                    save_list.append(line)
                else:
                    # remove wanted save
                    os.remove(line[0:len(line)-1])

                line = read.readline()
                count += 1

        # write the new saves
        with open(self.__SAVE_DIR_NAME+self.__SAVED_NAME+self.__SAVE_TYPE_NAME, 'w') as write_in:
            for i in save_list:
                write_in.write(i)

    def add_save_name_in_file(self):
        """ This function add the new save dir in saved_saves.txt """
        with open(self.__SAVE_DIR_NAME+self.__SAVED_NAME+self.__SAVE_TYPE_NAME, 'a') as write_in:
            write_in.write(self.__save_name+"\n")

    def set_save_data(self, player_data):
        """This function will write in name+_saves.txt and write the data

        The player_data is load in game_class .This data contains name,dmg
        hp,lvl,increase,reward,gold,exp range,hp range  , map , castle is 
        there , position , pet list , if have pet will write pet too.

        Args:
            player_data (list): name,dmg , hp,lvl,increase,reward,gold,exp
            range,hp range  , map , castle is there , position , pet list 
            ,if have pet will write pet
        """

        with open(self.get_save_name_dir(), 'w') as write:
            for i in player_data:
                write.write(str(i)+"\n")

    def set_save_name_dir(self, save_name):
        """This function set the curent self.__save_name to the given path

        Args:
            save_name (str): this the new path of name+saves_.txt
        """
        self.__save_name = save_name

    def get_save_name_dir(self):
        """This function return the curent  name+_saves.txt dir """
        return str(self.__save_name)

    def is_created(self, name):
        """This function check if the given name have name+_saves.txt

        Args:
            name (str): this is the player name

        Raises:
            FileNotFoundError:
                if file is not found that mean that the given name dont have
                save and save_name will be set to the new name+saves_.txt

        Returns:
            bool: if True thath mean the fail is created 
                False that mean the faile is not created
        """

        try:
            file_dir = self.__SAVE_DIR_NAME+name+self.__SAVE_TYPE_NAME
            open(file_dir, 'r')
            return True
        except FileNotFoundError:
            self.set_save_name_dir(file_dir)
            return False

    def get_player_dir_by_index(self, index):
        """This function will get the player dir by index


        Args:
            index (int): this is the index of the player

        Returns:
            str: this is the directory picked by the index

            False(bool): if the index is not equl to count will return False
        """
        # count the number of line saved dirs in saved_saves.txt
        count = 0

        with open(str(self.__SAVE_DIR_NAME+self.__SAVED_NAME+self.__SAVE_TYPE_NAME), 'r') as saves:
            line = saves.readline()
            while line != "":

                if count == index:
                    return line
                line = saves.readline()
                count += 1
            return False

    def get_player_by_index_data_list(self, index):
        """This function will get the player data with the given index

        Args:
            index (int): this is the player number

        Raises:
            FileNotFoundError: if the save some how is deleated in 
                saved_saves.txt will rais this error


        Returns:
            list: this is the data list of the player in the name+_saves.txt

            False(bool): if the index is not equal to count that mean the given
                index is out of range
        """

        """ This function will get player data by index  """

        count = 0
        load_data_list = []

        # read in saved_saves.txt this read the directory line of the name+_saves.txt
        with open(str(self.__SAVE_DIR_NAME+self.__SAVED_NAME+self.__SAVE_TYPE_NAME), 'r') as saves:
            line = saves.readline()
            while line != "":

                if count == index:
                    try:
                        # here will read the line in the name+_saves.txt
                        with open(line[0:-1], 'r') as player_save:
                            in_save_line = player_save.readline()
                            while in_save_line != '':
                                load_data_list.append(in_save_line)
                                in_save_line = player_save.readline()

                                # need to change this function
                                # when added new thing to player to save player data
                            return load_data_list
                    except FileNotFoundError as err:
                        print(
                            "get_player_by_index_data_list cant find this save", err)
                line = saves.readline()
                count += 1
            return False

    def create_save(self):
        """ This function create the player name+_saves.txt file 

        This function create the player name+_saves.txt file and add the 
        the path of the name+_saves.txt in saved_saves.txt
         """
        open(self.get_save_name_dir(), 'x')
        self.add_save_name_in_file()
#
