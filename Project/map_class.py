class Map:

    """ This class show the game map where player

    This class show the game map where player can move left to right,
    place a single castle and see enemy's castle


    Atributes:
        __CASTLE_SYMBOL(int): casle symbol default(2)

        __ENEMY_HERE(int): enemy symbol default(1)

        __NO_ENEMY(int): no enemy symbol default(0)

        __WELCOME_CASTLE(int): again castle symbol but used to check if the 
            player is in the castle default(2)

        __EXIT_CASTLE(int): when player leaf the castle default(2.5)


        __PLAYER_SYMBOL(str): this is the player symbol  default('*')

        __map(list): this is the map  
            default([self.__PLAYER_SYMBOL, 0, 0, 0, 0, 0, 1, 1])

        __player_position(int): player position defalut(0)

        __is_castle_there(bool): this is to check if casle is placed  defalut(False)

        __castle_pos(int): this is casle position default(0)

    Methods:

        def get_enemy_here():
            return __ENEMY_HERE

        def get_no_enemy():
            return __NO_ENEMY

        def get_welcome_castle():
            return __WELCOME_CASTLE

        def get_exit_castle():
            return __EXIT_CASTLE

        def set_player_position(position):
            set player position

        def set_castle_there(value):
            This function set is_castle_there value  

        def set_castle_position(pos):
            set casle position

        def get_castle_position():
            return __castle_pos

        def place_castle():
            this function will place the casle

        def is_castle_there():
            this function i to check i the is_castle_is there 

        def get_map_string_to_list(string):
            convert each char ot string to list

        def get_map_to_string():
            make __map to string 

        def set_map(map):
            set the map

        def get_map_len():
            return map len

        def get_map():
            Return the map 

        def get_player_position():
            return player position

        def get_player_position_from_map():
            Return the index of the player '*' 

        def move_right():
            this function move player

        def move_left():
            This function is identical as move_right

        def clear_map_position(position):
            this function will clear the previous '*' player symbol 

        def get_castle_symbol():
            This function will return the sumbil of the castle which is 2 

        def get_player_symbol():
            return str with player symbol

        def set_player_position_in_map(new_position, symbol):
            this function will set the new player position '*'  

     """
    __CASTLE_SYMBOL = 2

    __ENEMY_HERE = 1

    __NO_ENEMY = 0

    __WELCOME_CASTLE = 2

    __EXIT_CASTLE = 2.5

    def __init__(self):
        # this i player symbol
        self.__PLAYER_SYMBOL = '*'
        # player map
        self.__map = [self.__PLAYER_SYMBOL, 0, 0, 0, 0, 0, 1, 1]
        # this is the starting '*' player postition
        self.__player_position = 0
        self.__is_castle_there = False
        self.__castle_pos = 0

    def get_enemy_here(self):
        """return __ENEMY_HERE

        Returns:
            int: 1
        """
        return self.__ENEMY_HERE

    def get_no_enemy(self):
        """return __NO_ENEMY

        Returns:
            int: 0
        """
        return self.__NO_ENEMY

    def get_welcome_castle(self):
        """return __WELCOME_CASTLE

        Returns:
            int: 2
        """
        return self.__WELCOME_CASTLE

    def get_exit_castle(self):
        """return __EXIT_CASTLE

        Returns:
            int: 2.5
        """
        return self.__EXIT_CASTLE

    def set_player_position(self, position):
        """set player position

        Args:
            position (int): this is the positon int map

        Raises:
            ValueError:
                if the position isn't int

        """
        try:
            self.__player_position = int(position)
        except ValueError as err:
            print("set_player_position position must be an integer ", err)
        except:
            print("set_player_position unexpected error")

    def set_castle_there(self, value):
        """ This function set is_castle_there value  """
        try:
            self.__is_castle_there = bool(value)
        except NameError as err:
            print("set_castle_there need to be bool type",err)
        except:
            print("Opss... set_castle_there")

    def set_castle_position(self, pos):
        """set casle position

        Args:
            pos (int): set the casle pos
        """
        try:
            self.__castle_pos = int(pos)
        except ValueError as err:
            print("set_castle_position ",err)

    def get_castle_position(self):
        """return __castle_pos

        Returns:
            int: position of the calse
        """
        return self.__castle_pos

    def place_castle(self):
        """this function will place the casle
        """
        self.get_map()[self.get_player_position_from_map()
                       ] = self.__CASTLE_SYMBOL
        self.set_castle_position(self.get_player_position_from_map())
        # self.get_player_position()

    def is_castle_there(self):
        """ this function i to check i the is_castle_is there """
        return self.__is_castle_there

    def get_map_string_to_list(self, string):
        """convert each char ot string to list

        Args:
            string (str): this is the map

        Returns:
            list: this is the map as string
        """
        my_list = []
        for i in string:
            if i == '*':
                my_list.append(str(i))
            else:
                my_list.append(int(i))
        return my_list

    def get_map_to_string(self):
        """ make __map to string 

        Return:
            str: this is tha map as string
        """
        my_string = ""

        for i in self.get_map():
            my_string = my_string+str(i)
        return my_string

    def set_map(self, map):
        """set the map

        Args:
            map (list): list of int and string
        """
        self.__map = map

    def get_map_len(self):
        """return map len

        Returns:
            int: size of map
        """
        return len(self.get_map())

    def get_map(self):
        """ Return the map """
        return self.__map

    def get_player_position(self):
        """return player position

        Returns:
            int: the player pos
        """
        return self.__player_position

    def get_player_position_from_map(self):
        """ Return the index of the player '*' 
        use to for loop to check and if no '*" then player is in castle"""

        for i, v in enumerate(self.get_map()):
            if v == self.__PLAYER_SYMBOL:
                self.__player_position = i
            elif v == self.__CASTLE_SYMBOL:
                self.__player_position = i

        return self.__player_position

    def move_right(self):
        """this function move player

        If prev pos is castle the casle will stay and player pos will increse

        Returns:
            0,1,2,2.5: if next pos have no enemy return 0 
                if 1 then have enmy return 1
                if 2 there is castle return 2
                if 2.5 exit casle 
        """
        curr_pos = self.get_player_position()
        pl_sym = self.get_player_symbol()
        map = self.get_map()

        # you cant go out of the map
        if curr_pos+1 < len(map):

            # print("move_righ 1  curr_pos+1 < len(map)")
            # if enemy you will fight or no
            if map[curr_pos+1] == 1:
                # print("move_right map[curr_pos+1]==1")

                return self.get_enemy_here()

            # if the is casle you will enter the asle
            elif map[curr_pos+1] == 2:
                # print("move_righ 2.1 map[cur_pos+1]==2")
                print(self.__player_position)

                self.clear_map_position(curr_pos)
                self.set_player_position(curr_pos+1)
                # print(self.__player_position)
                return self.get_welcome_castle()

            # if mao[curr_pos+1] is 0 then you can move
            else:
                # print("move_right 2.2 else map[curr_pos+1] != 2")
                # but if prev pos is 2 you cant remove the caslte
                if map[curr_pos] == 2:
                    # print("move_righ 2.2.1 if map[curr_pos] == 2 ")
                    self.set_player_position(curr_pos+1)
                    self.set_player_position_in_map(curr_pos+1, pl_sym)
                    # print(self.__player_position)
                    return self.get_exit_castle()
                # if prev pos is 0 then you can clear prev pos and move to next pos
                else:
                    # print("move_righ 2.2.1 if map[curr_pos] !2= 2 ")
                    self.clear_map_position(curr_pos)
                    self.set_player_position(curr_pos+1)
                    self.set_player_position_in_map(curr_pos+1, pl_sym)
                    # print(self.__player_position)
                    return self.get_no_enemy()
        # you cant go out of the map
        else:
            pass

    def move_left(self):
        """This function is identical as move_right

        If prev pos is castle the casle will stay and player pos will increse

        Returns:
             0,1,2,2.5: if next pos have no enemy return 0 
                if 1 then have enmy return 1
                if 2 there is castle return 2
                if 2.5 exit casle 
        """
        curr_pos = self.get_player_position()
        pl_sym = self.get_player_symbol()
        map = self.get_map()

        if curr_pos-1 >= 0:
            # print("move left 1 curent_pos-1>=0")

            if map[curr_pos-1] == 2:
                # print("move_left 1.2.1 map[cur_pos-1]==2")
                self.clear_map_position(curr_pos)
                self.set_player_position(curr_pos-1)
                return self.get_welcome_castle()
            else:
                # print("move_left 1.2.2 map[cur_pos-1]!=2")

                if map[curr_pos] == 2:
                    # print("move_left 1.2.2.1 map[cur_pos]==2")
                    # print(self.__player_position)
                    self.set_player_position(curr_pos-1)
                    self.set_player_position_in_map(curr_pos-1, pl_sym)
                    # print(self.__player_position)
                    return self.get_exit_castle()
                else:
                    # print("move_left 1.2.2.2 map[cur_pos]!=2")
                    # print(self.__player_position)
                    self.clear_map_position(curr_pos)
                    self.set_player_position(curr_pos-1)
                    self.set_player_position_in_map(curr_pos-1, pl_sym)
                    # print(self.__player_position)
                    return self.get_no_enemy()

        else:
            pass

    def clear_map_position(self, position):
        """ this function will clear the previous '*' player symbol """
        self.get_map()[position] = 0

    def get_castle_symbol(self):
        """ This function will return the sumbil of the castle which is 2 """
        return self.__CASTLE_SYMBOL

    def get_player_symbol(self):
        """return str(self.__PLayer_SYBOL)

        Returns:
            str: player symbol
        """
        return str(self.__PLAYER_SYMBOL)

    def set_player_position_in_map(self, new_position, symbol):
        """ this function will set the new player position '*'  """
        try:
            self.get_map()[int(new_position)] = symbol
        except ValueError as err:
            print("set_player_position_in_map ",err)
