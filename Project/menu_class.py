class Menu():
    """ This class is to show list of thing you can do in diferent game states

    Atributes:
        __cost(int): default(50)
        __WATCH_FIGHT(list): default([
                                        "1.Next"
                                    ])
        __MAP_ATTACT(list): default([
                                    "1.Watch the fight",
                                    ])
        __CASTLE_MENU(list): default([
                                    "1.Buy pet",
                                    "2.Level up pet",
                                    ])
        __LOAD_GAME_MENU(list): default([
                                        "1.Load save index",
                                        "2.Remove save index",
                                        ])
        __PLAY_MENU(list): default([
                                    "1.Move left",
                                    "2.Move right",
                                    "3.Save",
                                    "4.Player stats",
                                    "5.Place a Castle"
                                    ])
        __GAME_MENU(list): default([
                                    "1.New game",
                                    "2.Load game"
                                    ])

    Methods:
        show_menu(menu):
            this function show menu
        get_map_attack_menu():
            return __MAP_ATTACT
        get_watch_fight_menu():
            return  __WATCH_FIGHT
        get_castle_menu():
            return  __CASTLE_MENU
        get_game_menu():
            return  __GAME_MENU
        get_load_game_menu():
            return  __LOAD_GAME_MENU
        get_playe_menu():
            return  __PLAY_MENU
        get_cost():
            return __cost
        show_castle_menu(menu, val):
            This will show castle menu
        show_play_menu(menu, val):
            this function will show the menu while playing the game 
    """

    __cost = 50

    __WATCH_FIGHT = [
        "1.Next"
    ]

    __MAP_ATTACT = [
        "1.Watch the fight",
    ]

    __CASTLE_MENU = [
        "1.Buy pet",
        "2.Level up pet",
    ]

    __LOAD_GAME_MENU = [
        "1.Load save index",
        "2.Remove save index",
    ]

    __PLAY_MENU = [
        "1.Move left",
        "2.Move right",
        "3.Save",
        "4.Player stats",
        "5.Place a Castle"
    ]

    __GAME_MENU = [
        "1.New game",
        "2.Load game"
    ]

    def show_menu(self, menu):
        """this function show menu

        Args:
            menu (list): this is the given menu
        """
        print()
        for i in menu:
            print(i)
        print("0.Exit\n")

    def get_map_attack_menu(self):
        """return __MAP_ATTACT

        Returns:
            list: this list contains strings
        """
        return self.__MAP_ATTACT

    def get_watch_fight_menu(self):
        """return  __WATCH_FIGHT

        Returns:
            list: return list which have string
        """
        return self.__WATCH_FIGHT

    def get_castle_menu(self):
        """return  __CASTLE_MENU

        Returns:
            list: return list which have string
        """
        return self.__CASTLE_MENU

    def get_game_menu(self):
        """return  __GAME_MENU

        Returns:
            list: return list which have string
        """
        return self.__GAME_MENU

    def get_load_game_menu(self):
        """return  __LOAD_GAME_MENU

        Returns:
            list: return list which have string
        """
        return self.__LOAD_GAME_MENU

    def get_playe_menu(self):
        """return  __PLAY_MENU

        Returns:
            list: return list which have string
        """
        return self.__PLAY_MENU

    def get_cost(self):
        """return __cost

        Returns:
            list: return list which have string
        """
        return self.__cost
    #

    def show_castle_menu(self, menu, val):
        """This will show castle menu

        Args:
            menu (list): this is the casle menu list

            val (bool): if the player buy pet the booll is try and show other
                half of the menu player can upgrae 
        """

        if bool(val):
            for i, v in enumerate(menu):
                print(v+" "+str(self.get_cost()) + " gold")
        else:
            for i in menu[0:-1]:
                print(i+" "+str(self.get_cost())+" gold")

        print("0.Exit\n")

    def show_play_menu(self, menu, val):
        """this function will show the menu while playing the game 

            if player place a castle menu will be reduced and 
            this menu will show all elements without the wast one 
            which is Place a castle

        Parameters:
            menu (list): must be the list __PLAY_MENU
            val (bool): if there is a castle will show  menu[0:-1]
                        else will show the full menu 
        """
        if val:
            self.show_menu(menu[0:-1])
        else:
            self.show_menu(menu)
