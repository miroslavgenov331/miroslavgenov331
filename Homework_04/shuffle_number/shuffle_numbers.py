
def shuffle_list_sides(side_list):

    number_move_1 = 0
    number_move_2 = 0

    for i in range(1, len(side_list)-1, 2):
        number_move_1 = side_list[i-1]
        number_move_2 = side_list[i]
        side_list[i] = number_move_1
        side_list[i-1] = number_move_2

    number_move_1 = side_list[len(side_list)//2]
    number_move_2 = side_list[-1]

    side_list[-1] = number_move_1
    side_list[len(side_list)//2] = number_move_2

    return side_list


def combine_sides(left_side, right_side):
    numbers_shuffled_combined_list = []
    for i in range(2):
        if i == 0:
            for j in left_side:
                numbers_shuffled_combined_list.append(j)
        else:
            for j in right_side:
                numbers_shuffled_combined_list.append(j)

    return numbers_shuffled_combined_list


numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
numbers_left_side = []
numbers_right_side = []
numbers_shuffled_combined_list = []

# need to slice the middle array in two separate arrays then
# use the sorting method and
# combine in new array


# shuffle
number_move_1 = numbers[-1]
number_move_2 = numbers[0]

numbers[-1] = number_move_2
numbers[0] = number_move_1

# split array
for i in range(0, len(numbers), len(numbers)//2):
    for j in range(i, len(numbers)//2+i, 1):
        if not j > len(numbers)//2-1:
            numbers_left_side.append(numbers[j])
        else:
            numbers_right_side.append(numbers[j])


numbers_left_side = shuffle_list_sides(numbers_left_side)
numbers_right_side = shuffle_list_sides(numbers_right_side)
numbers = combine_sides(numbers_left_side, numbers_right_side)


print(numbers)
