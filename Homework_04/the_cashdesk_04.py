class BillBatch:

    def __init__(self, bills=[]):
        self.bills = []

        for i in bills:
            self.bills.append(i)

    # return bills lenght
    def __len__(self):
        return len(self.bills)

    # return the index of the item
    def __getitem__(self, index):
        return self.bills[index]

    # return the total sum of the array
    def total(self):
        sum_result = 0
        for i in self.bills:
            # i.amouns
            sum_result = sum_result+i.amount
        return sum_result
# class BillBatch:


class Bill:

    # constructor
    def __init__(self, amount):
        try:
            if not isinstance(amount, int):
                raise TypeError("type of amount isn't int")
            elif int(amount) < 0:
                raise ValueError("amount is negative number")
        except TypeError as err_first:
            print("My TypeError: ", err_first)
        except ValueError as err_second:
            print("My ValueError: ", err_second)
        else:
            self.amount = amount

    # transfer int to strung and return string
    def __str__(self):
        return str(self.amount)

    def __repr__(self):
        return repr(''+str(self.amount))

    # return int of the amount

    def __int__(self):
        return int(self.amount)

    # if eqaul return true

    def __eq__(self, other):
        return self.amount == other.amount

    # return hash kay when used in dictionary

    def __hash__(self):
        return hash(self.amount)

# class Bill:


class CashDesk:
    def __init__(self):
        self.my_batch = []

    def total(self):
        sum_result = 0
        for i in self.my_batch:
            sum_result = sum_result+i
        return sum_result

    def take_money(self, batch):

        if isinstance(batch, BillBatch):
            for i in batch:
                self.my_batch.append(int(i))
        elif isinstance(batch, Bill):
            self.my_batch.append(int(batch))

    def inspec(self):
        next_bill = 0
        prev_bill = 0
        count_bill = 1

        for _ in range(len(self.my_batch)):
            for j in range(1, len(self.my_batch)):
                if self.my_batch[j-1] > self.my_batch[j]:
                    next_bill = self.my_batch[j-1]
                    prev_bill = self.my_batch[j]
                    self.my_batch[j-1] = prev_bill
                    self.my_batch[j] = next_bill

        print("We have a total of {}$ in the desk ".format(self.total()))
        for i in range(1, len(self.my_batch)):
            if self.my_batch[i-1] == self.my_batch[i]:
                count_bill += 1
            else:
                print("{}$ bills - {}".format(self.my_batch[i-1], count_bill))
                count_bill = 1
        print("{}$ bills - {}".format(self.my_batch[-1], count_bill))

# class CashDesk(BillBatch):


values = [10, 20, 50, 100, 100, 100]
bills = [Bill(value) for value in values]

batch = BillBatch(bills)

desk = CashDesk()

desk.take_money(batch)
desk.take_money(Bill(10))
desk.take_money(Bill(102))
print(desk.total())
desk.inspec()
