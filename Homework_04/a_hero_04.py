

# class Weapon:
class Weapon:

    # len(numbers_to_shuffle) needs to be even number
    numbers_to_shuffle = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]

    def __init__(self, weapon_type, damage, critical_strike_percent):
        self.weapon_type = weapon_type  # weapon type - axe , sword
        self.damage = damage  # if i hit how many hp i will reduce
        self.critical_strike_percent = float(0)

        if isinstance(critical_strike_percent, float):
            if critical_strike_percent > 0 and critical_strike_percent < 1:
                self.critical_strike_percent = critical_strike_percent
            else:
                if critical_strike_percent < 0:
                    critical_strike_percent = -critical_strike_percent
                critical_strike_percent = critical_strike_percent - \
                    int(critical_strike_percent)
                self.critical_strike_percent = critical_strike_percent

    # functions to shuffle list
    def __combine_sides(self, left_side, right_side):
        numbers_shuffled_combined_list = []
        for i in range(2):
            if i == 0:
                for j in left_side:
                    numbers_shuffled_combined_list.append(j)
            else:
                for j in right_side:
                    numbers_shuffled_combined_list.append(j)

        return numbers_shuffled_combined_list

    def __shuffle_list_side(self, side_list):

        number_move_1 = 0
        number_move_2 = 0

        for i in range(1, len(side_list)-1, 2):
            number_move_1 = side_list[i-1]
            number_move_2 = side_list[i]
            side_list[i] = number_move_1
            side_list[i-1] = number_move_2

        number_move_1 = side_list[len(side_list)//2]
        number_move_2 = side_list[-1]

        side_list[-1] = number_move_1
        side_list[len(side_list)//2] = number_move_2

        return side_list

    def __shuffle_numbers(self):
        numbers_left_side = []
        numbers_right_side = []

        number_move_1 = self.numbers_to_shuffle[-1]
        number_move_2 = self.numbers_to_shuffle[0]

        self.numbers_to_shuffle[-1] = number_move_2
        self.numbers_to_shuffle[0] = number_move_1

        # split array
        for i in range(0, len(self.numbers_to_shuffle), len(self.numbers_to_shuffle)//2):
            for j in range(i, len(self.numbers_to_shuffle)//2+i, 1):
                if not j > len(self.numbers_to_shuffle)//2-1:
                    numbers_left_side.append(self.numbers_to_shuffle[j])
                else:
                    numbers_right_side.append(self.numbers_to_shuffle[j])

        numbers_left_side = self.__shuffle_list_side(numbers_left_side)
        numbers_right_side = self.__shuffle_list_side(numbers_right_side)

        self.numbers_to_shuffle = self.__combine_sides(
            numbers_left_side, numbers_right_side)
    # end functions to shuffle list

    def critical_hit(self):
        self.__shuffle_numbers()

        if self.numbers_to_shuffle[0] > 4:
            return True
        else:
            return False
# class Weapon:


""" Entity_04.py """


class Entity:

    def __init__(self, name, health):
        self.name = name
        self.health = health
        self.maximum_health = health
        self.equiped_weapon = None

    def get_health(self):
        return self.health

    def is_alive(self):
        return not self.get_health() == 0

    def take_damage(self, damage_points):
        if damage_points > self.get_health():
            self.health = 0
        else:
            self.health = self.health-damage_points

    def take_healing(self, healing_points):

        if self.is_alive():
            # print("hello")
            if self.get_health()+healing_points > self.maximum_health:
                self.health = self.maximum_health
            else:
                self.health = self.health+healing_points
            return True
        else:
            return False

    def has_weapon(self):
        if not self.equiped_weapon == None:
            return True
        else:
            return False

    def equip_weapon(self, weapon):
        if isinstance(weapon, Weapon):
            self.equiped_weapon = weapon
            # print(self.equiped_weapon.weapon_type)

    def attack(self):
        if self.has_weapon():
            if self.equiped_weapon.critical_hit():

                crit_hit_damage = self.equiped_weapon.damage + \
                    self.equiped_weapon.damage*self.equiped_weapon.critical_strike_percent

                return crit_hit_damage
            else:
                return self.equiped_weapon.damage

        else:
            return 0
# class Entity


"""A_hero_04"""


class Hero(Entity):
    # Hero

    def __init__(self, name, health, nick_name):
        super().__init__(name, health)
        self.nick_name = nick_name

    def __str__(self):
        return "{} the {}".format(self.name, self.nick_name)
        # return self.name+" the " + self.nick_name
# class Hero


""" The_orc_04.py """


class Orc(Entity):

    def __init__(self, name, health, berserk_factor):
        super().__init__(name, health)
        self.maximum_health = health

        # berserk_factor algorithm
        if berserk_factor > 1 and berserk_factor < 2:
            self.berserk_factor = berserk_factor
        else:
            if berserk_factor < 0:
                berserk_factor = -berserk_factor
            berserk_factor = berserk_factor-int(berserk_factor) + 1
            self.berserk_factor = berserk_factor

    def attack(self):
        if self.has_weapon():
            if self.equiped_weapon.critical_hit():

                crit_hit_damage = self.equiped_weapon.damage * self.berserk_factor + \
                    self.equiped_weapon.damage*self.equiped_weapon.critical_strike_percent
                print("crit_hit_damage= ", crit_hit_damage)
                return crit_hit_damage
            else:
                print("normal dmg")
                return self.equiped_weapon.damage*self.berserk_factor

        else:
            return 0
# class Orc


humanoid_01 = Hero("Ivan", 100, "The DragonSlayer")
humanoid_01.equip_weapon(Weapon("Axe", 10, 0.8))


orc_01 = Orc("Blqbla", 120, 1.2)
orc_01.equip_weapon(Weapon("Axe", 10, 0.5))

humanoid_01.take_damage(orc_01.attack())
humanoid_01.take_damage(orc_01.attack())
humanoid_01.take_damage(orc_01.attack())
humanoid_01.take_healing(50)
humanoid_01.take_damage(orc_01.attack())
humanoid_01.take_damage(orc_01.attack())
humanoid_01.take_damage(orc_01.attack())
humanoid_01.take_damage(orc_01.attack())
humanoid_01.take_damage(orc_01.attack())
humanoid_01.take_damage(orc_01.attack())
print(humanoid_01.is_alive())
humanoid_01.take_damage(orc_01.attack())
print(humanoid_01.is_alive())

print(humanoid_01.get_health())
