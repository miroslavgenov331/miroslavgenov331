
"""

gas_stations(320, 90, [50, 80, 140, 180, 220, 290])
[80, 140, 220, 290]
gas_stations(390, 80, [70, 90, 140, 210, 240, 280, 350])
[70, 140, 210, 280, 350]

"""


def gas_station(distance, tank_size, stations):

    next_destionation = 0
    reach_this = int(distance)
    previous_destinations = []
    destination_save = 0
    stations.append(reach_this)
    distance_subtraction_result = 99999999999999999

    next_destionation = next_destionation+tank_size
    nums_in_sub_res = []
    count = 0

    while not next_destionation >= reach_this:
        for i in stations:
            if next_destionation-i >= 0:
                if next_destionation-i < distance_subtraction_result:
                    distance_subtraction_result = next_destionation-i
                    destination_save = i
                    nums_in_sub_res.append(i)

        for i in nums_in_sub_res:
            stations.remove(i)
        nums_in_sub_res = []

        # will save the greater subtraction from destination
        previous_destinations.append(destination_save)

        next_destionation = previous_destinations[count]+tank_size
        count += 1
        distance_subtraction_result = 999999999999999

    return previous_destinations


print(gas_station(320, 90, [50, 80, 140, 180, 220, 290]))
print(gas_station(390, 80, [70, 90, 140, 210, 240, 280, 350]))
