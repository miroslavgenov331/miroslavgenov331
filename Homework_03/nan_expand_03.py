
def nan_expand(number):

    zero_message = "\"\""

    if number < 0:
        number = -number

    if number == 0:
        return zero_message
    else:

        count = 0
        text = "Not a "
        word = "NaN"
        msg = ""

        while not count == number:
            count += 1
            msg = msg+text

        return msg+word


def nan_exapnd_version2(number):
    if number < 0:
        number = -number

    if number == 0:
        return "\"\""
    else:
        return "Not a "*number + "NaN"


# test
print(nan_expand(0))
print(nan_expand(1))
print(nan_expand(2))
print(nan_expand(3))

# print(nan_exapnd_version2(0))
# print(nan_exapnd_version2(1))
# print(nan_exapnd_version2(2))
# print(nan_exapnd_version2(3))
