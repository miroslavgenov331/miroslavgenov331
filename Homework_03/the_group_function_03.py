
def group(array_numbers):

    group = []
    equal = [""]
    count = 1
    my_bool = None

    for i in range(1, len(array_numbers)):

        if equal[0] != array_numbers[i]:
            if array_numbers[i-1] != array_numbers[i]:
                if i == len(array_numbers)-1:
                    group.append([array_numbers[i]])
                else:
                    if equal[0] != array_numbers[i-1]:
                        group.append([array_numbers[i-1]])
                        equal = [""]

        if array_numbers[i-1] == array_numbers[i] and equal[0] == "":
            my_bool = True
            equal = []
            for j in range(i, len(array_numbers)):
                if array_numbers[i-1] == array_numbers[j] and my_bool:
                    count += 1
                else:
                    my_bool = False

            for j in range(count):
                equal.append(array_numbers[i-1])
            group.append(equal)
            count = 1

    return group


print(group([1, 1, 1, 2, 3, 1, 1]))
print(group([1, 2, 1, 2, 3, 3]))
