
def sum_of_numbers(string_of_numbers):

    is_in_there = False
    numbers = "1234567890"
    sub_number_group = []
    group_number = []

    for i in string_of_numbers:
        for j in numbers:
            if i == j:
                is_in_there = True

        if is_in_there:
            sub_number_group.append(i)
            is_in_there = False
        else:
            # if not sub_number_group empty
            # group.append(sub_number_group) number in group
            if sub_number_group:
                group_number.append(sub_number_group)
                sub_number_group = []

    # if we are in the end of the list
    if sub_number_group:
        group_number.append(sub_number_group)
        sub_number_group = []

    numbers_list = []
    number = 0
    string_sum_result = ""

    if len(group_number) == 0:
        string_sum_result = "0"
        return string_sum_result
    elif len(group_number) == 1:
        for i in range(len(group_number)):
            for j in range(len(group_number[i])):
                string_sum_result = string_sum_result+str(group_number[i][j])
        return string_sum_result
    else:
        for i in range(len(group_number)):
            for j in range(len(group_number[i])):
                number = number*10
                number = number+int(group_number[i][j])
                # print(number)
            numbers_list.append(number)
            number = 0

        sum_numbers = 0
        string_char_plus = " + "
        string_char_eqaul = " = "

        for i in range(len(numbers_list)):
            sum_numbers = sum_numbers+numbers_list[i]
            # print(numbers_list[i])
            if i < len(numbers_list)-1:
                string_sum_result = string_sum_result + \
                    str(numbers_list[i])+string_char_plus
            if i == len(numbers_list)-1:
                string_sum_result = string_sum_result + \
                    str(numbers_list[i])+string_char_eqaul

        string_sum_result = string_sum_result+str(sum_numbers)

        return string_sum_result


print(sum_of_numbers("1111"))
print(sum_of_numbers("1abc33xy22"))
print(sum_of_numbers("abcd"))
