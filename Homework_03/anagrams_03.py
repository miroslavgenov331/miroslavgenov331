def is_anagram(fst_word, snd_word):

    if not len(fst_word) == len(snd_word):
        return False
    else:

        fst_word_char_list = []
        snd_word_char_list = []

        fst_word = fst_word.lower()
        snd_word = snd_word.lower()

        fst_word_char_list = string_to_character_list(
            fst_word, fst_word_char_list)

        snd_word_char_list = string_to_character_list(
            snd_word, snd_word_char_list)

        fst_word_char_list = sort_word_char_list(fst_word_char_list)

        snd_word_char_list = sort_word_char_list(snd_word_char_list)

        for i in range(len(fst_word_char_list)):
            # compare if the char are equals or not
            if not fst_word_char_list[i] == snd_word_char_list[i]:
                return False
        return True


def string_to_character_list(word, word_char_list):
    for i in range(len(word)):
        word_char_list.append(word[i])
    return word_char_list


def sort_word_char_list(word_char_list):
    """ Sot_word char list """
    for i in range(len(word_char_list), 0, -1):
        for j in range(1, i, 1):
            if word_char_list[j-1] > word_char_list[j]:
                next_word = word_char_list[j-1]
                previous_word = word_char_list[j]
                word_char_list[j] = next_word
                word_char_list[j-1] = previous_word

    return word_char_list
# def sort_word_char_list(word_char_list)


# test
print(is_anagram("BREAD", "BeaRD"))
print(is_anagram("TOP_CODER", "COTO_PRODE"))
