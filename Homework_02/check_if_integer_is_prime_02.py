# 3.Check if integer is prime_02

def is_prime(number):

    # if number is 0 || 1 not prime
    if number == 0 or number == 1:
        return False
    # if

    for i in range(2, number):
        # check if have remainder = false
        if number % i == 0:
            return False
        # if
    # for

    # if no remainder number is prime
    return True
# is_prime(number)


# test
print(is_prime(1))
print(is_prime(2))
print(is_prime(8))
print(is_prime(11))
print(is_prime(-10))
