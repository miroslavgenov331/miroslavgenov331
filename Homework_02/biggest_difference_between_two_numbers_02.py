# 8.Biggest difference between two number_02

def biggest_difference(array):

    difference_result = 0

    for i in range(len(array)):
        for j in range(len(array)):
            if array[i]-array[j] < difference_result:
                difference_result = array[i]-array[j]
            # if
        # for
    # for

    return difference_result
# def biggest_difference(arr)


# test
print(biggest_difference([1, 2]))
print(biggest_difference([1, 2, 3, 4, 5]))
print(biggest_difference([-10, -9, -1]))
print(biggest_difference(range(0, 100)))
