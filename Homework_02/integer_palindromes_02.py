# 4.Integer Palindromes_02

#-----to_digit -----#
def to_digit(number):
    digit_list = []
    my_number = int(number)

    while not my_number == 0:
        digit_list.append(my_number % 10)
        my_number = my_number//10
    # while number_!=0

    return digit_list
# end def to_digit(number)


#-----to_number -----#
def to_number(digit_list):
    number_ten = 10
    digits_to_number = 0

    for i in range(len(digit_list)):
        digits_to_number = digits_to_number*number_ten
        digits_to_number = digits_to_number+digit_list[i]
    # for i

    return digits_to_number
# end def to_number(digit_list)


#-----is_int_palindrome -----#
def is_int_palindrome(number):

    # 1) to digit will make return list of digits
    # 2) to_number will make the list of digits to number
    # 3) then will return the resumt in number_reverse
    number_revers = int(to_number(to_digit(number)))

    if number == number_revers:
        return True
    # if

    return False
# end def is_int_palindrome(number)


def main():
    # test

    print(is_int_palindrome(1))
    print(is_int_palindrome(42))
    print(is_int_palindrome(100001))
    print(is_int_palindrome(999))
    print(is_int_palindrome(123))


main()
