#10.What is the sign_02

def what_is_my_sign(day, month):
    
    dictionary={
        "Aries": [3, 21, 4, 20],
        "Tauris": [4, 21, 5, 21],
        "Gemini": [5, 22, 6, 21],
        "Cancer": [6, 22, 7, 22],
        "Leo": [7, 23, 8, 22],
        "Virgo": [8, 23, 9, 23],
        "Libra": [9, 24, 10, 23],
        "Scorpio": [10, 24, 11, 22],
        "Sagittarius": [11, 23, 12, 21],
        "Capricorn": [12, 22, 1, 20],
        "Aquarius": [1, 21, 2, 19],
        "Pisces": [2, 20, 3, 20]
        }

    date_list=list()

    for i in dictionary:
        # adding first item in list to get the list from dictionary
        
        date_list.extend(dictionary[i])
        
        if month == date_list[0]:

            if day >= date_list[1]:
                return i
        
        if month == date_list[2]:
            if day <= date_list[3]:
                return i

        date_list=list()


print(what_is_my_sign(5,8))
print(what_is_my_sign(29,1))
print(what_is_my_sign(30,6))
print(what_is_my_sign(31,5))
print(what_is_my_sign(2,2))
print(what_is_my_sign(8,5))
print(what_is_my_sign(9,1))