# 7.Counting substring_92


def count_substrings(haystack, needle):

    # count_result whill count thre result
    count_result = 0
    # array_index_count will count for the char needle
    array_index_count = 0

    for i in range(len(haystack)):

        # if the first needle[0] is trye then array_index ++ and
        # continue
        # if array_index equals len(needle) this meean that it is
        # like all the chars match and count
        if needle[array_index_count] == haystack[i]:

            array_index_count += 1

            if array_index_count == len(needle):
                count_result += 1
                array_index_count = 0
            # if array_index_count
        else:
            array_index_count = 0
        # if needle[array_index_count]

    # for
    return count_result
# def count_substrings(haystack, needle):


# test
print(count_substrings("This is a test string", "is"))
print(count_substrings("babababa", "baba"))
print(count_substrings("Python is anawesome language to program in!", "o"))
print(count_substrings("We have nothing in common!", "really?"))
print(count_substrings("This is this and that is this", "this"))
