# 5.Number containing a single digit_02

def to_digit_reverse(number):
    """ To_digit_reverse function """
    digit_list = []

    while number:
        digit_list.append(number % 10)
        number = number//10
    # while

    return digit_list[::-1]
# def to_digit_reverse(number)


def contains_digit(number, digit):

    if digit in to_digit_reverse(number):
        return True
    # if
    return False
# def contains_digit(number,digit)


# test
print(contains_digit(123, 4))
print(contains_digit(42, 2))
print(contains_digit(1000, 0))
print(contains_digit(12346789, 5))
