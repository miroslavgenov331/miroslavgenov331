# 1.Sum the minimum and maximum elements_02

def sum_of_min_and_max(array):

    if array:
        big_number = 0
        small_number = 0
        sum_result = 0

        # sorting the array
        for _ in range(len(array)):
            for j in range(1, len(array)):
                if array[j-1] > array[j]:
                    big_number = array[j-1]
                    small_number = array[j]
                    array[j-1] = small_number
                    array[j] = big_number
                # if
            # for j
        # for i

        sum_result = array[0]+array[len(array)-1]
        return sum_result
    # if
    else:
        return 0
    # else
# def sum_of_min_and_max(array)


# test
print(sum_of_min_and_max([1, 2, 3, 4, 5, 6, 7, 8, 9]))
print(sum_of_min_and_max([-10, 5, 10, 100]))
print(sum_of_min_and_max([1]))
