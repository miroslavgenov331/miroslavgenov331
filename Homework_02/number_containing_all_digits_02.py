# 6.Number containing all digits?_02

def to_digit(number):
    digit_list = []

    while number:
        digit_list.append(number % 10)
        number = number//10
    # while

    return digit_list
# def to_digit(number)


def contains_digits(number, digits):
    number_in_digit = to_digit(number)

    for i in range(len(digits)):
        if digits[i] not in number_in_digit:
            return False
        # if
    # for

    return True
# def contains_digits(number,digits)


# test
print(contains_digits(402123, [0, 3, 4]))
print(contains_digits(666, [6, 4]))
print(contains_digits(123456789, [1, 2, 3, 0]))
print(contains_digits(456, []))
